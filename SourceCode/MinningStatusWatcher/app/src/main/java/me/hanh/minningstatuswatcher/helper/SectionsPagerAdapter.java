package me.hanh.minningstatuswatcher.helper;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import me.hanh.minningstatuswatcher.activities.fragments.DashboardFragment;
import me.hanh.minningstatuswatcher.activities.fragments.PayoutFragment;
import me.hanh.minningstatuswatcher.activities.fragments.SettingsFragment;
import me.hanh.minningstatuswatcher.activities.fragments.WorkerFragment;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        //return MainFragment.newInstance(position + 1);
        switch (position) {
            case 0:
                return DashboardFragment.newInstance("Dashboard","Fragement");
            case 1:
                return WorkerFragment.newInstance("Worker","Fragement");
            case 2:
                return PayoutFragment.newInstance("Payout","Fragement");
            case 3:
                return SettingsFragment.newInstance("Settings","Fragement");
            default:
                break;
        }
        return new Fragment();
    }

    @Override
    public int getCount() {
        // Show total 2 pager
        return 4;
    }



}