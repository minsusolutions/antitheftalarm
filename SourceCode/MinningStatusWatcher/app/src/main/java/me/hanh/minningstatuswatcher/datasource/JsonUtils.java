package me.hanh.minningstatuswatcher.datasource;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class JsonUtils {

    public static Object findJsonObject(JSONObject jsonObject, String arrayName) {
        try {
            Iterator<?> iterator = jsonObject.keys();
            while (iterator.hasNext()) {
                String obj = iterator.next().toString();

                if (obj.equals(arrayName)) {
                    return jsonObject.get(obj);
                }

                if (jsonObject.get(obj) instanceof JSONObject) {
                    return findJsonObject((JSONObject) jsonObject.get(obj),arrayName);
                }

                if (jsonObject.get(obj) instanceof JSONArray) {
                    JSONArray jsonArray = (JSONArray) jsonObject.get(obj);
                    for(int n = 0; n < jsonArray.length(); n++)
                    {
                        JSONObject jb = jsonArray.getJSONObject(n);
                        findJsonObject(jb,arrayName);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new JSONArray();
    }
}
