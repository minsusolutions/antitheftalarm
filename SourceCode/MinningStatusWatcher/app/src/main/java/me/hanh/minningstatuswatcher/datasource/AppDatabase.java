package me.hanh.minningstatuswatcher.datasource;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.TypeConverter;
import android.content.Context;

import android.arch.persistence.room.RoomDatabase;
import android.support.annotation.NonNull;

import java.sql.Date;
import java.util.List;

import me.hanh.minningstatuswatcher.datasource.models.MinerDAO;
import me.hanh.minningstatuswatcher.datasource.models.MinerEntity;
import me.hanh.minningstatuswatcher.datasource.models.Statistics;
import me.hanh.minningstatuswatcher.datasource.models.StatisticsDAO;
import me.hanh.minningstatuswatcher.datasource.models.Worker;
import me.hanh.minningstatuswatcher.datasource.models.WorkerDAO;

@Database(entities = {MinerEntity.class,Worker.class, Statistics.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {


    private static AppDatabase instance;
    private static final String DATABASE_NAME = "mining-pool-status-database";

    public abstract MinerDAO minerDao();
    public abstract WorkerDAO workerDao();
    public abstract StatisticsDAO statisticsDao();


    private final MutableLiveData<Boolean> mIsDatabaseCreated = new MutableLiveData<>();

    public static AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context,AppDatabase.class,DATABASE_NAME).allowMainThreadQueries().build();
                    //                    instance.updateDatabaseCreated(context.getApplicationContext());
        }
        return instance;
    }

//    /**
//     * Build the database. {@link Builder#build()} only sets up the database configuration and
//     * creates a new instance of the database.
//     * The SQLite database is only created when it's accessed for the first time.
//     */
//    private static AppDatabase buildDatabase(final Context appContext,
//                                             final AppExecutors executors) {
//        return Room.databaseBuilder(appContext, AppDatabase.class, DATABASE_NAME)
//                .addCallback(new Callback() {
//                    @Override
//                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
//                        super.onCreate(db);
//                        executors.diskIO().execute(() -> {
//                            // Add a delay to simulate a long-running operation
////                            addDelay();
//                            // Generate the data for pre-population
//                            AppDatabase database = AppDatabase.getInstance(appContext, executors);
//                            // notify that the database was created and it's ready to be used
//                            database.setDatabaseCreated();
//                        });
//                    }
//                }).allowMainThreadQueries().build();
//    }

    /**
     * Check whether the database already exists and expose it via {@link #getDatabaseCreated()}
     */
    private void updateDatabaseCreated(final Context context) {
        if (context.getDatabasePath(DATABASE_NAME).exists()) {
            setDatabaseCreated();
        }
    }

    private void setDatabaseCreated(){
        mIsDatabaseCreated.postValue(true);
    }



    //Miner
    public void insertData(final MinerEntity miner) {
        this.minerDao().insertAll(miner);
    }

    public LiveData<MinerEntity> getMinerData() {
       // LiveData<MinerDAO> data = new LiveData<MinerDAO>()
        return this.minerDao().loadMiner();
    }




    // Worker
    public void insertWorkerData(final List<Worker> workers) {
            this.workerDao().insertAll(workers);
    }

    public LiveData<List<Worker>> loadWorkers() {
        return this.workerDao().loadWorkers();
    }


    public LiveData<Boolean> getDatabaseCreated() {
        return mIsDatabaseCreated;
    }

    public static void destroyInstance() {
        instance = null;
    }

}


