package me.hanh.minningstatuswatcher.datasource.models;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;
@Dao
public interface StatisticsDAO {

    @Query("SELECT * FROM statistics")
    LiveData<List<Statistics>> loadStatistics();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Statistics> statistics);

//    @Query("select * from products where id = :productId")
//    LiveData<ProductEntity> loadProduct(int productId);
//
//    @Query("select * from products where id = :productId")
//    ProductEntity loadProductSync(int productId);
//
}