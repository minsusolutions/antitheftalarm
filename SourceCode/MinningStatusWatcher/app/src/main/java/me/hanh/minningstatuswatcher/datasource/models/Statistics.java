package me.hanh.minningstatuswatcher.datasource.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.database.sqlite.SQLiteDatabase;

import java.util.Date;

import me.hanh.minningstatuswatcher.datasource.TimestampConverter;

@Entity(tableName = "statistics")
public class Statistics {
    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "time")
    @TypeConverters({TimestampConverter.class})
    private Date time;

    @ColumnInfo(name = "reportedHashrate")
    private double reportedHashrate;

    @ColumnInfo(name = "currentHashrate")
    private double currentHashrate;

    @ColumnInfo(name = "validShares")
    private int  validShares;

    @ColumnInfo(name = "invalidShare")
    private int invalidShare;

    @ColumnInfo(name = "staleShares")
    private int staleShares;

    @ColumnInfo(name = "activeWorkers")
    private int activeWorkers;

    public Statistics(Date time, double reportedHashrate, double currentHashrate, int validShares, int invalidShare, int staleShares, int activeWorkers) {
        this.time = time;
        this.reportedHashrate = reportedHashrate;
        this.currentHashrate = currentHashrate;
        this.validShares = validShares;
        this.invalidShare = invalidShare;
        this.staleShares = staleShares;
        this.activeWorkers = activeWorkers;
    }


    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public double getReportedHashrate() {
        return reportedHashrate;
    }

    public void setReportedHashrate(double reportedHashrate) {
        this.reportedHashrate = reportedHashrate;
    }

    public double getCurrentHashrate() {
        return currentHashrate;
    }

    public void setCurrentHashrate(double currentHashrate) {
        this.currentHashrate = currentHashrate;
    }

    public int getValidShares() {
        return validShares;
    }

    public void setValidShares(int validShares) {
        this.validShares = validShares;
    }

    public int getInvalidShare() {
        return invalidShare;
    }

    public void setInvalidShare(int invalidShare) {
        this.invalidShare = invalidShare;
    }

    public int getStaleShares() {
        return staleShares;
    }

    public void setStaleShares(int staleShares) {
        this.staleShares = staleShares;
    }

    public int getActiveWorkers() {
        return activeWorkers;
    }

    public void setActiveWorkers(int activeWorkers) {
        this.activeWorkers = activeWorkers;
    }
}

//public class Statistics {
//
//
//    /* sample data
//        "time": 1523382600,
//                "reportedHashrate": 1982018150,
//                "currentHashrate": 1895333333.3333333,
//                "validShares": 1672,
//                "invalidShares": 0,
//                "staleShares": 52,
//                "activeWorkers": 11
//                */
//
//
//    public static final String MINER_TABLE_NAME = "statistics";
//
//    public static final String MINER_COLUMN_TIME = "time";
//    public static final String MINER_COLUMN_REPORTED_HASHRATE = "reported_hashrate";
//    public static final String MINER_COLUMN_CURRENT_HASHRATE = "current_hashrate";
//    public static final String MINER_COLUMN_VALID_SHARES = "valid_share";
//    public static final String MINER_COLUMN_INVALID_SHARES = "invalid_share";
//    public static final String MINER_COLUMN_STALE_SHARES = "stale_share";
//    public static final String MINER_COLUMN_ACTIVE_WORKERS = "active_workers";
//
//
//    public static final String CREATE_MINER_TABLE =
//            "CREATE TABLE " + MINER_TABLE_NAME + "("
//                    + MINER_COLUMN_TIME + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
//                    + MINER_COLUMN_REPORTED_HASHRATE + " REAL,"
//                    + MINER_COLUMN_CURRENT_HASHRATE + " REAL,"
//                    + MINER_COLUMN_VALID_SHARES + " INTEGER,"
//                    + MINER_COLUMN_INVALID_SHARES + " INTEGER,"
//                    + MINER_COLUMN_STALE_SHARES + " INTEGER,"
//                    + MINER_COLUMN_ACTIVE_WORKERS + " INTEGER"
//                    + ")";
//
//    public static class Model extends DatabaseController.LocalDatabaseModel {
//
//        public Model(){
//        }
//
//        @Override
//        public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){
//            //Implement update logic for this model/table
//        }
//        @Override
//        public void onCreate(SQLiteDatabase database){
//            //Implement create logic for this model/table
//            database.execSQL(CREATE_MINER_TABLE);
//        }
//    }
//}
