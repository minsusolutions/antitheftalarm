package me.hanh.minningstatuswatcher.datasource.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.database.sqlite.SQLiteDatabase;

import java.sql.Timestamp;
import java.util.Date;

import me.hanh.minningstatuswatcher.datasource.TimestampConverter;

@Entity(tableName = "miner")
public class MinerEntity {

    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "time")
    @TypeConverters({TimestampConverter.class})
    private Date time;

    @ColumnInfo(name = "lastSeen")
    @TypeConverters({TimestampConverter.class})
    private Date lastSeen;

    @ColumnInfo(name = "reportedHashrate")
    private double reportedHashrate;

    @ColumnInfo(name = "currentHashrate")
    private double currentHashrate;

    @ColumnInfo(name = "validShares")
    private int  validShares;

    @ColumnInfo(name = "invalidShare")
    private int invalidShare;

    @ColumnInfo(name = "staleShares")
    private int staleShares;

    @ColumnInfo(name = "activeWorkers")
    private int activeWorkers;

    @ColumnInfo(name = "unpaid")
    private double unpaid;


    public MinerEntity(Date time, Date lastSeen, double reportedHashrate, double currentHashrate, int validShares, int invalidShare, int staleShares, int activeWorkers, double unpaid) {
        this.time = time;
        this.lastSeen = lastSeen;
        this.reportedHashrate = reportedHashrate;
        this.currentHashrate = currentHashrate;
        this.validShares = validShares;
        this.invalidShare = invalidShare;
        this.staleShares = staleShares;
        this.activeWorkers = activeWorkers;
        this.unpaid = unpaid;
    }


    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    public double getReportedHashrate() {
        return reportedHashrate;
    }

    public void setReportedHashrate(double reportedHashrate) {
        this.reportedHashrate = reportedHashrate;
    }

    public double getCurrentHashrate() {
        return currentHashrate;
    }

    public void setCurrentHashrate(double currentHashrate) {
        this.currentHashrate = currentHashrate;
    }

    public int getValidShares() {
        return validShares;
    }

    public void setValidShares(int validShares) {
        this.validShares = validShares;
    }

    public int getInvalidShare() {
        return invalidShare;
    }

    public void setInvalidShare(int invalidShare) {
        this.invalidShare = invalidShare;
    }

    public int getStaleShares() {
        return staleShares;
    }

    public void setStaleShares(int staleShares) {
        this.staleShares = staleShares;
    }

    public int getActiveWorkers() {
        return activeWorkers;
    }

    public void setActiveWorkers(int activeWorkers) {
        this.activeWorkers = activeWorkers;
    }

    public double getUnpaid() {
        return unpaid;
    }

    public void setUnpaid(double unpaid) {
        this.unpaid = unpaid;
    }
}




















//public final class Miner {
//
//    private Timestamp time;
//    private Timestamp lastSeen;
//    private double reportedHashrate;
//    private double currentHashrate;
//    private int  validShares;
//    private int invalidShare;
//    private int staleShares;
//    private int activeWorkers;
//    private String minerAddress;
//    private double unpaid;
//
//    public static final String TAG = "MINER";
//
//    /* sample data
//        currentStatistics": {
//                "time": 1523411400,
//                "lastSeen": 1523411400,
//                "reportedHashrate": 1980363253,
//                "currentHashrate": 1867277777.7777777,
//                "validShares": 1650,
//                "invalidShares": 0,
//                "staleShares": 47,
//                "activeWorkers": 11,
//                "unpaid": 212908815662705440
//                */
//
//
//    public static final String MINER_TABLE_NAME = "miner";
//
//    public static final String MINER_COLUMN_TIME = "time";
//    public static final String MINER_COLUMN_LASTSEEN = "lastseen";
//    public static final String MINER_COLUMN_REPORTED_HASHRATE = "reported_hashrate";
//    public static final String MINER_COLUMN_CURRENT_HASHRATE = "current_hashrate";
//    public static final String MINER_COLUMN_VALID_SHARES = "valid_share";
//    public static final String MINER_COLUMN_INVALID_SHARES = "invalid_share";
//    public static final String MINER_COLUMN_STALE_SHARES = "stale_share";
//    public static final String MINER_COLUMN_ACTIVE_WORKERS = "active_workers";
//    public static final String MINER_COLUMN_UNPAID = "unpaid";
//
//
//    public static final String CREATE_MINER_TABLE =
//            "CREATE TABLE " + MINER_TABLE_NAME + "("
//                    + MINER_COLUMN_TIME + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
//                    + MINER_COLUMN_LASTSEEN + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
//                    + MINER_COLUMN_REPORTED_HASHRATE + " REAL,"
//                    + MINER_COLUMN_CURRENT_HASHRATE + " REAL,"
//                    + MINER_COLUMN_VALID_SHARES + " INTEGER,"
//                    + MINER_COLUMN_INVALID_SHARES + " INTEGER,"
//                    + MINER_COLUMN_STALE_SHARES + " INTEGER,"
//                    + MINER_COLUMN_ACTIVE_WORKERS + " INTEGER,"
//                    + MINER_COLUMN_UNPAID + " REAL"
//                    + ")";
//
//
//
//
//
//
//
//    public static class Model extends DatabaseController.LocalDatabaseModel {
//
//        public Model(){
//        }
//
//        @Override
//        public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){
//            //Implement update logic for this model/table
//        }
//        @Override
//        public void onCreate(SQLiteDatabase database){
//            //Implement create logic for this model/table
//            database.execSQL(CREATE_MINER_TABLE);
//        }
//    }
//}
