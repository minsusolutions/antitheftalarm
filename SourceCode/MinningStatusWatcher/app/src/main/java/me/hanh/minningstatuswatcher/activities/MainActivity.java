package me.hanh.minningstatuswatcher.activities;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.util.Log;
import android.view.View;

import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import me.hanh.minningstatuswatcher.R;
//import me.hanh.minningstatuswatcher.datasource.DatabaseController;
import me.hanh.minningstatuswatcher.datasource.HttpUtils;
import me.hanh.minningstatuswatcher.datasource.MinerDataSource;
import me.hanh.minningstatuswatcher.datasource.network.NetworkConnection;
import me.hanh.minningstatuswatcher.helper.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String MINER_ADDRESS = "0x0a53971fcc97c058355100bd0ee1248fd890783d";

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;


    private AppCompatImageButton mDashboardBtn;
    private AppCompatImageButton mWorkerBtn;
    private AppCompatImageButton mPayoutBtn;
    private AppCompatImageButton mSettingsBtn;
    private AppCompatImageButton mRefreshBtn;


    private static final String BASE_URL = "networkStats";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupViews();

        setupEvents();

        setupDatabase();


        MinerDataSource.getInstance(getApplicationContext(),MINER_ADDRESS).fetchMinerData();
        //MinerDataSource.getInstance(getApplicationContext(),MINER_ADDRESS).updateMinerData();

//        NetworkConnection nc = new NetworkConnection(getApplicationContext());




//        HttpUtils.post(AppConstant.URL_FEED, rp, new JsonHttpResponseHandler() {
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//                // If the response is JSONObject instead of expected JSONArray
//                Log.d("asd", "---------------- this is response : " + response);
//                try {
//                    JSONObject serverResp = new JSONObject(response.toString());
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
//                // Pull out the first event on the public timeline
//
//            }
//        });

    }


    private void setupViews() {

        mViewPager = findViewById(R.id.viewPager);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mDashboardBtn = findViewById(R.id.dashboardBtn);
        mWorkerBtn = findViewById(R.id.workerBtn);
        mPayoutBtn = findViewById(R.id.payoutBtn);
        mSettingsBtn = findViewById(R.id.settingBtn);
        mRefreshBtn = findViewById(R.id.refreshBtn);

    }

    private void setupEvents() {

        mDashboardBtn.setOnClickListener(this);
        mWorkerBtn.setOnClickListener(this);
        mPayoutBtn.setOnClickListener(this);
        mSettingsBtn.setOnClickListener(this);
        mRefreshBtn.setOnClickListener(this);

    }

    private void setupDatabase() {

//        DatabaseController db = DatabaseController.getInstance(getApplicationContext());
//
//        db.openDatabase();
//
//        Miner miner = new Miner(MINER_ADDRESS);
//        miner.fetchMinerData();
//
//        db.close();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.dashboardBtn:
                mViewPager.setCurrentItem(0);
                break;

            case R.id.workerBtn:
                mViewPager.setCurrentItem(1);
                break;

            case R.id.payoutBtn:
                mViewPager.setCurrentItem(2);
                break;
            case R.id.settingBtn:
                mViewPager.setCurrentItem(3);
                break;
            case R.id.refreshBtn:
                break;

            default:
                break;
        }
    }


}
