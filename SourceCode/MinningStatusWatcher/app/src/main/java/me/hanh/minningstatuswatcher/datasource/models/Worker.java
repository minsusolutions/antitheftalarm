package me.hanh.minningstatuswatcher.datasource.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.database.sqlite.SQLiteDatabase;

import java.sql.Timestamp;
import java.util.Date;

import me.hanh.minningstatuswatcher.datasource.TimestampConverter;

@Entity(tableName = "worker")
public class Worker {

    public static final String WORKER_COLUMN_WORKER_NAME = "worker";
    public static final String WORKER_COLUMN_TIME = "time";
    public static final String WORKER_COLUMN_LAST_SEEN = "lastSeen";
    public static final String WORKER_COLUMN_REPORTED_HASHRATE = "reportedHashrate";
    public static final String WORKER_COLUMN_CURRENT_HASHRATE = "currentHashrate";
    public static final String MINER_COLUMN_VALID_SHARES = "validShares";
    public static final String MINER_COLUMN_INVALID_SHARES = "invalidShares";
    public static final String MINER_COLUMN_STALE_SHARES = "staleShares";
    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "worker")
    private String worker;

    @ColumnInfo(name = "time")
    @TypeConverters({TimestampConverter.class})
    private Date time;

    @ColumnInfo(name = "lastSeen")
    @TypeConverters({TimestampConverter.class})
    private Date lastSeen;

    @ColumnInfo(name = "reportedHashrate")
    private double reportedHashrate;

    @ColumnInfo(name = "currentHashrate")
    private double currentHashrate;

    @ColumnInfo(name = "validShares")
    private int  validShares;

    @ColumnInfo(name = "invalidShare")
    private int invalidShare;

    @ColumnInfo(name = "staleShares")
    private int staleShares;

    public Worker(String worker, Date time, Date lastSeen, double reportedHashrate, double currentHashrate, int validShares, int invalidShare, int staleShares) {
        this.worker = worker;
        this.time = time;
        this.lastSeen = lastSeen;
        this.reportedHashrate = reportedHashrate;
        this.currentHashrate = currentHashrate;
        this.validShares = validShares;
        this.invalidShare = invalidShare;
        this.staleShares = staleShares;
    }

    public String getWorker() {
        return worker;
    }

    public void setWorker(String worker) {
        this.worker = worker;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    public double getReportedHashrate() {
        return reportedHashrate;
    }

    public void setReportedHashrate(double reportedHashrate) {
        this.reportedHashrate = reportedHashrate;
    }

    public double getCurrentHashrate() {
        return currentHashrate;
    }

    public void setCurrentHashrate(double currentHashrate) {
        this.currentHashrate = currentHashrate;
    }

    public int getValidShares() {
        return validShares;
    }

    public void setValidShares(int validShares) {
        this.validShares = validShares;
    }

    public int getInvalidShare() {
        return invalidShare;
    }

    public void setInvalidShare(int invalidShare) {
        this.invalidShare = invalidShare;
    }

    public int getStaleShares() {
        return staleShares;
    }

    public void setStaleShares(int staleShares) {
        this.staleShares = staleShares;
    }
}
//
//
//public final class Worker {
//
//    public static final String WORKER_TABLE_NAME = "worker";
//
//    public static final String WORKER_COLUMN_WORKER_NAME = "worker_name";
//    public static final String WORKER_COLUMN_TIME = "time";
//    public static final String WORKER_COLUMN_LAST_SEEN = "last_seen";
//    public static final String WORKER_COLUMN_REPORTED_HASHRATE = "reported_hashrate";
//    public static final String WORKER_COLUMN_CURRENT_HASHRATE = "current_hashrate";
//    public static final String WORKER_COLUMN_AVERAGE_HASHRATE = "average_hashrate";
//    public static final String MINER_COLUMN_VALID_SHARES = "valid_share";
//    public static final String MINER_COLUMN_INVALID_SHARES = "invalid_share";
//    public static final String MINER_COLUMN_STALE_SHARES = "stale_share";
//
//
//
//    /*
//
//                "worker": "binh5",
//                "time": 1523411400,
//                "lastSeen": 1523411361,
//                "reportedHashrate": 176700253,
//                "currentHashrate": 172111111.1111111,
//                "validShares": 151,
//                "invalidShares": 0,
//                "staleShares": 6
//
//    */
//
//
//
//    public static final String CREATE_WORKER_TABLE =
//            "CREATE TABLE " + WORKER_TABLE_NAME + "("
//                    + WORKER_COLUMN_WORKER_NAME + " TEXT,"
//                    + WORKER_COLUMN_TIME + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
//                    + WORKER_COLUMN_LAST_SEEN + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
//                    + WORKER_COLUMN_REPORTED_HASHRATE + " REAL,"
//                    + WORKER_COLUMN_CURRENT_HASHRATE + " REAL,"
//                    + WORKER_COLUMN_AVERAGE_HASHRATE + " REAL,"
//                    + MINER_COLUMN_VALID_SHARES + " INTEGER,"
//                    + MINER_COLUMN_INVALID_SHARES + " INTEGER,"
//                    + MINER_COLUMN_STALE_SHARES + " INTEGER"
//                    + ")";
//
//    private String workerName;
//    private Timestamp time;
//    private Timestamp lastSeen;
//
//    private double reportedHashrate;
//    private double currentHashrate;
//    private double averageHashrate;
//
//    private int validShares;
//    private int invalidShares;
//    private int staleShares;
//
//
//
//    public Worker(String title){
//    }
//
//    private Worker(long id, String title){
//    }
//
//
//    public void save(DatabaseController db){
//        //save or update the book, throw an exception on failure.
//    }
//
//    //More non static methods (getters, setters, database methods) here
//
//    public static Worker getById(DatabaseController db, long id){
//        //Do select query and get an existing book from the database.
//        return new Worker("hello");
//    }
//
//    //More static methods here
//
//    public static class Model extends DatabaseController.LocalDatabaseModel {
//
//        public Model(){
//        }
//
//        @Override
//        public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){
//            //Implement update logic for this model/table
//        }
//        @Override
//        public void onCreate(SQLiteDatabase database){
//            //Implement create logic for this model/table
//            database.execSQL(CREATE_WORKER_TABLE);
//        }
//    }
//}