package me.hanh.minningstatuswatcher.datasource.models;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface WorkerDAO {

    @Query("SELECT * FROM worker")
    LiveData<List<Worker>> loadWorkers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Worker> worker);

    @Query("select * from worker where worker = :workerName")
    LiveData<Worker> loadWorker(int workerName);

//    @Query("select * from products where id = :productId")
//    ProductEntity loadProductSync(int productId);
//
}
