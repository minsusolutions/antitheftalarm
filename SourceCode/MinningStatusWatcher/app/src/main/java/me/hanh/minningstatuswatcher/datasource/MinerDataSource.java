package me.hanh.minningstatuswatcher.datasource;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import cz.msebera.android.httpclient.Header;
import me.hanh.minningstatuswatcher.datasource.models.MinerEntity;
import me.hanh.minningstatuswatcher.datasource.models.Worker;

public class MinerDataSource {

    private static final String DASHBOARD = "/dashboard";
    private static final String MINER = "miner/";

    private static final String TAG = "MinerDataSource";

    private static MinerDataSource instance = null;
    private Context ctx;
    private String minerAddress;

    public JSONObject minerData = new JSONObject();
    public JSONArray workersData = new JSONArray();

    // Add you LocalDatabaseModels here.

    public synchronized static MinerDataSource getInstance(Context context, String minerAddress) {
        if (instance == null) {
            instance = new MinerDataSource(context.getApplicationContext(), minerAddress);
        }
        return instance;
    }

    public MinerDataSource(Context ctx, String minerAddress) {
        this.minerAddress = minerAddress;
        this.ctx = ctx;
    }



    private String buildDashboardUrl() {
        return MINER + this.minerAddress + DASHBOARD;
    }


    public void fetchMinerData() {
        RequestParams rp = new RequestParams();
        //rp.add("username", "aaa"); rp.add("password", "aaa@123");

        if (minerAddress != null && !minerAddress.trim().equalsIgnoreCase("")) {
            HttpUtils.get(buildDashboardUrl(), rp, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    super.onSuccess(statusCode, headers, responseString);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    // If the response is JSONObject instead of expected JSONArray
                    Log.d(TAG, "---------------- this is response : " + response);
                    try {
                        JSONObject serverResp = new JSONObject(response.toString());

                        workersData = (JSONArray) JsonUtils.findJsonObject(serverResp,"workers");
//
//                        Object statistics = JsonUtils.findJsonObject(serverResp,"statistics");
                        minerData = (JSONObject) JsonUtils.findJsonObject(serverResp,"currentStatistics");

                        updateMinerData();
                        //AppDatabase.getAppDatabase(ctx).insertData();
                        updateWorkerDatabase();
                        getWorkers();

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                }
            });

        } else {
            Log.d(TAG,"Miner address is null");
        }
    }

    public void updateMinerData() {

        try {
            Date time = new  Date(minerData.getLong("time") * 1000);
            Date lastSeen = new Date(minerData.getLong("lastSeen") * 1000);

            double reportedHashrate = minerData.getDouble("reportedHashrate");
            double currentHashrate = minerData.getDouble("currentHashrate");

            int validShare = minerData.getInt("validShares");
            int staleShares = minerData.getInt("staleShares");
            int invalidShares = minerData.getInt("invalidShares");

            int activeWorkers = minerData.getInt("activeWorkers");

            double unpaid = minerData.getDouble("unpaid");
    //public MinerEntity(Date time, Date lastSeen, double reportedHashrate, double currentHashrate, int validShares, int invalidShare, int staleShares, int activeWorkers, String minerAddress, double unpaid) {


            AppDatabase appDatabase = AppDatabase.getInstance(ctx);
            appDatabase.insertData(new MinerEntity(time,lastSeen,reportedHashrate,currentHashrate,validShare,invalidShares,staleShares,activeWorkers,unpaid));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }





    private void updateWorkerDatabase() {
        List<Worker> workers = new ArrayList<>();
        try {
            for(int n = 0; n < workersData.length(); n++)
            {

//                "worker": "binhz10603g",
//                    "time": 1523411400,
//                    "lastSeen": 1523411397,
//                    "reportedHashrate": 184492398,
//                    "currentHashrate": 169611111.1111111,
//                    "validShares": 152,
//                    "invalidShares": 0,
//                    "staleShares": 1
                JSONObject jb = workersData.getJSONObject(n);

                String workerName = jb.getString(Worker.WORKER_COLUMN_WORKER_NAME);
                Date time = new Date(jb.getLong(Worker.WORKER_COLUMN_TIME) * 1000);
                Date lastSeen = new Date(jb.getLong(Worker.WORKER_COLUMN_LAST_SEEN) * 1000);
                double reportedHashrate = jb.getDouble(Worker.WORKER_COLUMN_REPORTED_HASHRATE);
                double currentHashrate = jb.getDouble(Worker.WORKER_COLUMN_CURRENT_HASHRATE);
                int validShare = jb.getInt(Worker.MINER_COLUMN_VALID_SHARES);
                int invalidShare = jb.getInt(Worker.MINER_COLUMN_INVALID_SHARES);
                int staleShare = jb.getInt(Worker.MINER_COLUMN_STALE_SHARES);

                workers.add(new Worker(workerName,time,lastSeen,reportedHashrate,currentHashrate,validShare,invalidShare,staleShare));
            }

            if (!workers.isEmpty()) {
                AppDatabase.getInstance(ctx).workerDao().insertAll(workers);
                AppDatabase.getInstance(ctx).insertWorkerData(workers);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getWorkers() {
        LiveData<List<Worker>> workers = AppDatabase.getInstance(ctx).workerDao().loadWorkers();
    }


}
