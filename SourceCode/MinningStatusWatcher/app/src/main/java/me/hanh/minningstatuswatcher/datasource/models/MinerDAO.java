package me.hanh.minningstatuswatcher.datasource.models;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface MinerDAO {

    @Query("SELECT * FROM miner")
    LiveData<MinerEntity> loadMiner();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(MinerEntity miner);

//    @Query("select * from products where id = :productId")
//    LiveData<ProductEntity> loadProduct(int productId);
//
//    @Query("select * from products where id = :productId")
//    ProductEntity loadProductSync(int productId);
//
}
