package antitheftalarm.hanhnv.com.antitheftalarm.datasources;

import java.util.ArrayList;

/**
 * Created by Admin on 2/23/2018.
 */

public class ActionDataSource {
    public ArrayList<AlarmAction> getAlarmActions() {
        return alarmActions;
    }

    public ArrayList<Mode> getModes() {
        return modes;
    }

    private ArrayList<AlarmAction> alarmActions;
    private ArrayList<Mode> modes;

    public ActionDataSource(ArrayList<AlarmAction> alarmActions, ArrayList<Mode> modes) {
        this.alarmActions = alarmActions;
        this.modes = modes;
    }

}
