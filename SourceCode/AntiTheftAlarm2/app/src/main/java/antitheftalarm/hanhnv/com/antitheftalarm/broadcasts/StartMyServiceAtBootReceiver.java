package antitheftalarm.hanhnv.com.antitheftalarm.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import antitheftalarm.hanhnv.com.antitheftalarm.activities.SplashActivity;

/**
 * Created by hanhnv on 2/23/18.
 */

public class StartMyServiceAtBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent activityIntent = new Intent(context, SplashActivity.class);
            activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(activityIntent);
            Toast.makeText(context,"MyApplicationUp",Toast.LENGTH_LONG).show();
        }
    }
}
