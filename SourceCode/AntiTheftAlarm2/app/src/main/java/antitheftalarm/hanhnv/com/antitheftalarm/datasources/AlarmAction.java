package antitheftalarm.hanhnv.com.antitheftalarm.datasources;

import android.drm.DrmStore;

import java.io.Serializable;

/**
 * Created by Admin on 2/23/2018.
 */


public class AlarmAction implements Serializable {
    private ActionType actionType;
    private String actionName;
    private ActionStatus actionStatus;
    private String actionDescription;
    private boolean isAutomatically;
    private double timeAutomation;
    private String imageName;

    public AlarmAction(ActionType actionType, String actionName, ActionStatus actionStatus, String actionDescription, boolean isAutomatically, double timeAutomation, String imageName) {
        this.actionType = actionType;
        this.actionName = actionName;
        this.actionStatus = actionStatus;
        this.actionDescription = actionDescription;
        this.isAutomatically = isAutomatically;
        this.timeAutomation = timeAutomation;
        this.imageName = imageName;
    }


    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public ActionStatus getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(ActionStatus actionStatus) {
        this.actionStatus = actionStatus;
    }

    public String getActionDescription() {
        return actionDescription;
    }

    public void setActionDescription(String actionDescription) {
        this.actionDescription = actionDescription;
    }

    public boolean isAutomatically() {
        return isAutomatically;
    }

    public void setAutomatically(boolean automatically) {
        isAutomatically = automatically;
    }

    public double getTimeAutomation() {
        return timeAutomation;
    }

    public void setTimeAutomation(double timeAutomation) {
        this.timeAutomation = timeAutomation;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
