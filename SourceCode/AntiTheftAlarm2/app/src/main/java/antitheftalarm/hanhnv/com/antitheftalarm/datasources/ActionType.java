package antitheftalarm.hanhnv.com.antitheftalarm.datasources;

/**
 * Created by hanhnv on 3/1/18.
 */

public enum ActionType {
        CHARGER,
        PROXIMITY,
        MOTION,
        SIMCARD
}

