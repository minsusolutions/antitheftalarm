package antitheftalarm.hanhnv.com.antitheftalarm.datasources;

import java.sql.Timestamp;

public class Mode {

    private String modeName;
    private String imageName;
    private boolean isScheduled = false;
    private boolean isMonday = false;
    private boolean isTuesday = false;
    private boolean isWednesday = false;
    private boolean isThursday = false;
    private boolean isFriday = false;
    private boolean isSaturday = false;
    private boolean isSunday = false;
    private Timestamp fromTime;
    private Timestamp toTime;
    private boolean isCharger = false;
    private boolean isMotion = false;
    private boolean isProximity = false;
    private boolean isSimcard = false;


    public Mode(String modeName, String imageName) {
        this.modeName = modeName;
        this.imageName = imageName;
    }

    public Mode(String modeName, String imageName, boolean isCharger, boolean isMotion, boolean isProximity, boolean isSimcard) {
        this.modeName = modeName;
        this.imageName = imageName;
        this.isCharger = isCharger;
        this.isMotion = isMotion;
        this.isProximity = isProximity;
        this.isSimcard = isSimcard;
    }

    public String getModeName() {
        return modeName;
    }

    public String getImageName() {
        return imageName;
    }

    public boolean isScheduled() {
        return isScheduled;
    }

    public boolean isMonday() {
        return isMonday;
    }

    public boolean isTuesday() {
        return isTuesday;
    }

    public boolean isWednesday() {
        return isWednesday;
    }

    public boolean isThursday() {
        return isThursday;
    }

    public boolean isFriday() {
        return isFriday;
    }

    public boolean isSaturday() {
        return isSaturday;
    }

    public boolean isSunday() {
        return isSunday;
    }

    public Timestamp getFromTime() {
        return fromTime;
    }

    public Timestamp getToTime() {
        return toTime;
    }

    public boolean isCharger() {
        return isCharger;
    }

    public boolean isMotion() {
        return isMotion;
    }

    public boolean isProximity() {
        return isProximity;
    }

    public boolean isSimcard() {
        return isSimcard;
    }


    public void setModeName(String modeName) {
        this.modeName = modeName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public void setScheduled(boolean scheduled) {
        isScheduled = scheduled;
    }

    public void setMonday(boolean monday) {
        isMonday = monday;
    }

    public void setTuesday(boolean tuesday) {
        isTuesday = tuesday;
    }

    public void setWednesday(boolean wednesday) {
        isWednesday = wednesday;
    }

    public void setThursday(boolean thursday) {
        isThursday = thursday;
    }

    public void setFriday(boolean friday) {
        isFriday = friday;
    }

    public void setSaturday(boolean saturday) {
        isSaturday = saturday;
    }

    public void setSunday(boolean sunday) {
        isSunday = sunday;
    }

    public void setFromTime(Timestamp fromTime) {
        this.fromTime = fromTime;
    }

    public void setToTime(Timestamp toTime) {
        this.toTime = toTime;
    }

    public void setCharger(boolean charger) {
        isCharger = charger;
    }

    public void setMotion(boolean motion) {
        isMotion = motion;
    }

    public void setProximity(boolean proximity) {
        isProximity = proximity;
    }

    public void setSimcard(boolean simcard) {
        isSimcard = simcard;
    }
}
