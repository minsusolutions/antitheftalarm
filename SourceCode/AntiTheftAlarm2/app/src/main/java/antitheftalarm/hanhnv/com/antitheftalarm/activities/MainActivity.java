package antitheftalarm.hanhnv.com.antitheftalarm.activities;

import android.app.ActionBar;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import antitheftalarm.hanhnv.com.antitheftalarm.R;
import antitheftalarm.hanhnv.com.antitheftalarm.datasources.ActionDataSource;
import antitheftalarm.hanhnv.com.antitheftalarm.datasources.ActionStatus;
import antitheftalarm.hanhnv.com.antitheftalarm.datasources.ActionType;
import antitheftalarm.hanhnv.com.antitheftalarm.datasources.AlarmAction;
import antitheftalarm.hanhnv.com.antitheftalarm.datasources.LocalPersistence;
import antitheftalarm.hanhnv.com.antitheftalarm.datasources.Mode;
import antitheftalarm.hanhnv.com.antitheftalarm.datasources.ModeListAdapter;
import antitheftalarm.hanhnv.com.antitheftalarm.utils.ConsVal;
import antitheftalarm.hanhnv.com.antitheftalarm.utils.DisplayInfo;
import antitheftalarm.hanhnv.com.antitheftalarm.utils.SharedPreferenceHelper;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,SwitchCompat.OnCheckedChangeListener, View.OnClickListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private TextView mNavigationTitle;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    ConstraintLayout chargerView;
    TextView chargerMainText;
    TextView chargerSubText;
    SwitchCompat chargerSwitch;
    ImageView chargerImage;

    ConstraintLayout motionView;
    TextView motionMainText;
    TextView motionSubText;
    SwitchCompat motionSwitch;
    ImageView motionImage;

    ConstraintLayout proximityView;
    TextView proximityMainText;
    TextView proximitySubText;
    SwitchCompat proximitySwitch;
    ImageView proximityImage;

    ConstraintLayout simcardView;
    TextView simcardMainText;
    TextView simcardSubText;
    SwitchCompat simcardSwitch;
    ImageView simcardImage;
    private ActionDataSource dataSource;

    private ArrayList<AlarmAction> alarmActions;

    private MaterialDialog addModeDialog;
    private TextView fromTime;
    private TextView toTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prepareViews();
        setupViews();
        setupGridViews();
        setupMethodViews();

    }

    private void setupGridViews() {
        List<Mode> image_details = dataSource.getModes();
        final GridView gridView = (GridView) findViewById(R.id.gridview);
        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = getModeListHeight();
        gridView.setLayoutParams(params);
        gridView.requestLayout();
        gridView.setAdapter(new ModeListAdapter(this, image_details));
        gridView.setOnItemClickListener(this);
    }

    private void setupViews() {
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        final android.support.v7.app.ActionBar bar =  this.getSupportActionBar();

        window.setStatusBarColor(ContextCompat.getColor(this, R.color.tab_text_color_highlighted));
        bar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.tab_text_color_highlighted)));
        bar.setTitle(R.string.app_title);


        if (SharedPreferenceHelper.getInstance(this).getFirstRunAppValue()) {
            SharedPreferenceHelper.getInstance(this).saveFirstRunAppToPref();

            ArrayList alarmActions = new ArrayList<>();
            alarmActions.add(new AlarmAction(ActionType.CHARGER, getResources().getString(R.string.charger_main_text), ActionStatus.DISABLED,getResources().getString(R.string.charger_sub_text),false,0.0,"charger.png"));
            alarmActions.add(new AlarmAction(ActionType.MOTION, getResources().getString(R.string.motion_main_text),ActionStatus.ENABLED,getResources().getString(R.string.motion_sub_text),false,0.0,"motion.png"));
            alarmActions.add(new AlarmAction(ActionType.PROXIMITY, getResources().getString(R.string.proximity_main_text),ActionStatus.DISABLED,getResources().getString(R.string.proximity_sub_text),false,0.0,"proximity.png"));
            alarmActions.add(new AlarmAction(ActionType.SIMCARD, getResources().getString(R.string.simcard_main_text),ActionStatus.ENABLED,getResources().getString(R.string.simcard_sub_text),false,0.0,"simcard.png"));


            ArrayList<Mode> modes = new ArrayList<>();

            Mode mode1 = new Mode("On the bus ", "colored_moon");
            Mode mode2 = new Mode("Good night", "sun_set");
            Mode mode3 = new Mode("Hello world, babe gdafdafdadafdafaifjeiwjaoifjdoiajfoijdajfdas", "sun_rise");


            modes.add(mode1);
            modes.add(mode2);
            modes.add(mode3);
            modes.add(mode1);
            modes.add(mode2);
            modes.add(mode3);

            dataSource = new ActionDataSource(alarmActions,modes);
            SharedPreferenceHelper.getInstance(this).saveActionList(dataSource);
        }

        dataSource = SharedPreferenceHelper.getInstance(this).getAlarmActionList();
        this.alarmActions = dataSource.getAlarmActions();

    }


    private void prepareViews() {

        chargerView = findViewById(R.id.chargerView);
        chargerMainText = findViewById(R.id.chargerModeTitle);
        chargerSubText = findViewById(R.id.chargerDescription);
        chargerSwitch = findViewById(R.id.chargerSwitch);
        chargerImage = findViewById(R.id.chargerImage);

        //chargerSwitch.setThumbTintList(myList);
        //chargerSwitch.setTrackTintList(myList2);
        motionView = findViewById(R.id.motionView);
        motionMainText = findViewById(R.id.motionModeTitle);
        motionSubText = findViewById(R.id.motionDescription);
        motionSwitch = findViewById(R.id.motionSwitch);
        motionImage = findViewById(R.id.motionImage);

        proximityView = findViewById(R.id.proximityView);
        proximityMainText = findViewById(R.id.proximityModeTitle);
        proximitySubText = findViewById(R.id.proximityDescription);
        proximitySwitch = findViewById(R.id.proximitySwitch);
        proximityImage = findViewById(R.id.proximityImage);

        simcardView = findViewById(R.id.simcardView);
        simcardMainText = findViewById(R.id.simcardModeTitle);
        simcardSubText = findViewById(R.id.simcardDescription);
        simcardSwitch = findViewById(R.id.simcardSwitch);
        simcardImage = findViewById(R.id.simcardImage);

        chargerSwitch.setOnCheckedChangeListener(this);
        motionSwitch.setOnCheckedChangeListener(this);
        proximitySwitch.setOnCheckedChangeListener(this);
        simcardSwitch.setOnCheckedChangeListener(this);


        boolean wrapInScrollView = true;

        addModeDialog = new MaterialDialog.Builder(this)
                .title("Add mode")
                .canceledOnTouchOutside(false)
                .customView(R.layout.add_mode_dialog, wrapInScrollView)
                .positiveText("OK")
                .negativeText("Cancel").build();

        View dialogView = addModeDialog.getCustomView();
//        CheckBox addShortcut = dialogView.findViewById(R.id.addShortcut);
//        addShortcut.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    //timepickder.setVisibility(View.VISIBLE);
//                } else {
//                    //timepickder.setVisibility(View.GONE);
//                }
//            }
//        });

        /*
        final ConstraintLayout timepickder = dialogView.findViewById(R.id.timepicker);
        timepickder.setVisibility(View.GONE);

        isScheduled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    timepickder.setVisibility(View.VISIBLE);
                } else {
                    timepickder.setVisibility(View.GONE);
                }
            }
        });

        fromTime = dialogView.findViewById(R.id.fromTime);
        toTime = dialogView.findViewById(R.id.toTime);

        fromTime.setOnClickListener(this);
        toTime.setOnClickListener(this);

        */

    }

    private void setupMethodViews() {
        for (AlarmAction action : alarmActions) {
            switch (action.getActionType()) {
                case CHARGER:

                    chargerMainText.setText(action.getActionName());
                    chargerSubText.setText(action.getActionDescription());

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        chargerSwitch.setChecked(true);
                    } else {
                        chargerSwitch.setChecked(false);
                    }

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        chargerMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                        chargerSubText.setTextColor(getResources().getColor(R.color.subTextOn));
                        chargerImage.setImageResource(R.drawable.charger_unhighlighted);
                        TransitionDrawable transition = (TransitionDrawable) chargerView.getBackground();
                        transition.startTransition(150);
                    } else {
                        chargerMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                        chargerSubText.setTextColor(getResources().getColor(R.color.subTextOff));
                        chargerImage.setImageResource(R.drawable.charger_highlighted);
                        TransitionDrawable transition = (TransitionDrawable) chargerView.getBackground();
                        transition.startTransition(1);
                        transition.reverseTransition(150);
                    }

                    break;
                case MOTION:
                    motionMainText.setText(action.getActionName());
                    motionSubText.setText(action.getActionDescription());

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        motionSwitch.setChecked(true);
                    } else {
                        motionSwitch.setChecked(false);
                    }

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        motionMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                        motionSubText.setTextColor(getResources().getColor(R.color.subTextOn));
                        motionImage.setImageResource(R.drawable.motion_unhighlighted);
                        TransitionDrawable transition = (TransitionDrawable) motionView.getBackground();
                        transition.startTransition(150);
                    } else {
                        motionMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                        motionSubText.setTextColor(getResources().getColor(R.color.subTextOff));
                        motionImage.setImageResource(R.drawable.motion_highlighted);
                        TransitionDrawable transition = (TransitionDrawable) motionView.getBackground();
                        transition.startTransition(1);
                        transition.reverseTransition(150);
                    }
                    break;
                case PROXIMITY:
                    proximityMainText.setText(action.getActionName());
                    proximitySubText.setText(action.getActionDescription());

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        proximitySwitch.setChecked(true);
                    } else {
                        proximitySwitch.setChecked(false);
                    }

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        proximityMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                        proximitySubText.setTextColor(getResources().getColor(R.color.subTextOn));
                        proximityImage.setImageResource(R.drawable.proximity_unhightlighted);
                        TransitionDrawable transition = (TransitionDrawable) proximityView.getBackground();
                        transition.startTransition(150);
                    } else {
                        proximityMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                        proximitySubText.setTextColor(getResources().getColor(R.color.subTextOff));
                        proximityImage.setImageResource(R.drawable.proximity_hightlighted);
                        TransitionDrawable transition = (TransitionDrawable) proximityView.getBackground();
                        transition.startTransition(1);
                        transition.reverseTransition(150);
                    }
                    break;
                case SIMCARD:
                    simcardMainText.setText(action.getActionName());
                    simcardSubText.setText(action.getActionDescription());

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        simcardSwitch.setChecked(true);
                    } else {
                        simcardSwitch.setChecked(false);
                    }

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        simcardMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                        simcardSubText.setTextColor(getResources().getColor(R.color.subTextOn));
                        simcardImage.setImageResource(R.drawable.simcard_unhightlighted);

                        TransitionDrawable transition = (TransitionDrawable) simcardView.getBackground();
                        transition.startTransition(150);
                    } else {
                        simcardMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                        simcardSubText.setTextColor(getResources().getColor(R.color.subTextOff));
                        simcardImage.setImageResource(R.drawable.simcard_hightlighted);
                        TransitionDrawable transition = (TransitionDrawable) simcardView.getBackground();
                        transition.startTransition(1);
                        transition.reverseTransition(150);
                    }
                    break;
                default:
                    break;
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_add_mode) {
            addModeDialog.show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void centerTitle() {
        ArrayList<View> textViews = new ArrayList<>();

        getWindow().getDecorView().findViewsWithText(textViews, getTitle(), View.FIND_VIEWS_WITH_TEXT);

        if(textViews.size() > 0) {
            AppCompatTextView appCompatTextView = null;
            if(textViews.size() == 1) {
                appCompatTextView = (AppCompatTextView) textViews.get(0);
            } else {
                for(View v : textViews) {
                    if(v.getParent() instanceof Toolbar) {
                        appCompatTextView = (AppCompatTextView) v;
                        break;
                    }
                }
            }

            if(appCompatTextView != null) {
                ViewGroup.LayoutParams params = appCompatTextView.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                appCompatTextView.setLayoutParams(params);
                appCompatTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        }
    }

    private List<Mode> getListData() {
        List<Mode> list = new ArrayList<Mode>();

        Mode mode1 = new Mode("On the bus ", "colored_moon");
        Mode mode2 = new Mode("Good night", "sun_set");
        Mode mode3 = new Mode("Hello world, babe gdafdafdadafdafaifjeiwjaoifjdoiajfoijdajfdas", "sun_rise");


        list.add(mode1);
        list.add(mode2);
        list.add(mode3);
        list.add(mode1);
        list.add(mode2);
        list.add(mode3);

        return list;
    }

    private int getModeListHeight () {
        int count = getListData().size();

        if (count > 0) {
            DisplayInfo displayInfo = new DisplayInfo(this);
            int line = count/2 +  count %2;
            return (int)((line * 60 + (line - 1)*10)*displayInfo.getDensity());
        }

        return 0;

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        TransitionDrawable transition = (TransitionDrawable) view.getBackground();
        transition.startTransition(2000);
        transition.reverseTransition(1500);
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.chargerSwitch:
                changeCardStatus(ActionType.CHARGER,b);
                break;
            case R.id.motionSwitch:
                changeCardStatus(ActionType.MOTION,b);
                break;
            case R.id.proximitySwitch:
                changeCardStatus(ActionType.PROXIMITY,b);
                break;
            case R.id.simcardSwitch:
                changeCardStatus(ActionType.SIMCARD,b);
                break;

            default:
                break;
        }
    }


    private void changeCardStatus(ActionType actionType, boolean activated) {
        switch (actionType) {
            case CHARGER:

                if (activated) {
                    chargerMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                    chargerSubText.setTextColor(getResources().getColor(R.color.subTextOn));
                    chargerImage.setImageResource(R.drawable.charger_unhighlighted);
                    TransitionDrawable transition = (TransitionDrawable) chargerView.getBackground();
                    transition.startTransition(150);
                } else {
                    chargerMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                    chargerSubText.setTextColor(getResources().getColor(R.color.subTextOff));
                    chargerImage.setImageResource(R.drawable.charger_highlighted);
                    //chargerView.setBackground(getResources().getDrawable(R.drawable.card_gradient_off));
                    TransitionDrawable transition = (TransitionDrawable) chargerView.getBackground();
                    transition.reverseTransition(150);
                }

                break;
            case MOTION:

                if (activated) {
                    motionMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                    motionSubText.setTextColor(getResources().getColor(R.color.subTextOn));
                    motionImage.setImageResource(R.drawable.motion_unhighlighted);

                    TransitionDrawable transition = (TransitionDrawable) motionView.getBackground();
                    transition.startTransition(150);
                } else {
                    motionMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                    motionSubText.setTextColor(getResources().getColor(R.color.subTextOff));
                    motionImage.setImageResource(R.drawable.motion_highlighted);

                    TransitionDrawable transition = (TransitionDrawable) motionView.getBackground();
                    transition.reverseTransition(150);
                }
                break;
            case PROXIMITY:


                if (activated) {
                    proximityMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                    proximitySubText.setTextColor(getResources().getColor(R.color.subTextOn));
                    proximityImage.setImageResource(R.drawable.proximity_unhightlighted);

                    TransitionDrawable transition = (TransitionDrawable) proximityView.getBackground();
                    transition.startTransition(150);
                } else {
                    proximityMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                    proximitySubText.setTextColor(getResources().getColor(R.color.subTextOff));
                    proximityImage.setImageResource(R.drawable.proximity_hightlighted);

                    TransitionDrawable transition = (TransitionDrawable) proximityView.getBackground();
                    transition.reverseTransition(150);
                }
                break;
            case SIMCARD:


                if (activated) {
                    simcardMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                    simcardSubText.setTextColor(getResources().getColor(R.color.subTextOn));
                    simcardImage.setImageResource(R.drawable.simcard_unhightlighted);

                    TransitionDrawable transition = (TransitionDrawable) simcardView.getBackground();
                    transition.startTransition(150);
                } else {
                    simcardMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                    simcardSubText.setTextColor(getResources().getColor(R.color.subTextOff));
                    simcardImage.setImageResource(R.drawable.simcard_hightlighted);
                    TransitionDrawable transition = (TransitionDrawable) simcardView.getBackground();
                    transition.reverseTransition(150);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View view) {
        /*Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;

        switch (view.getId()) {
            case R.id.fromTime:

                mTimePicker = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        fromTime.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

                break;
            case R.id.toTime:

                mTimePicker = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        toTime.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

                break;
                default:
                    return;
        }*/
    }
}
