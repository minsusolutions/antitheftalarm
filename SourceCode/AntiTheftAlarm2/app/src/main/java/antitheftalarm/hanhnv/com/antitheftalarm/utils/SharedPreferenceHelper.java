package antitheftalarm.hanhnv.com.antitheftalarm.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;

import antitheftalarm.hanhnv.com.antitheftalarm.datasources.ActionDataSource;
import antitheftalarm.hanhnv.com.antitheftalarm.datasources.AlarmAction;

/**
 * Created by Admin on 2/23/2018.
 */

public class SharedPreferenceHelper {

    private static final String APP_PREFERENCES = "PREFS";
    private static final String ALARM_ACTIONS = "ALARM_ACTIONS";
    private static final String FIRST_RUN = "FIRST_RUN";
    private static final String IS_PASSCODED = "IS_PASSCODED";
    private static final String PASSCODE = "PASSCODE";

    private static SharedPreferenceHelper instance = null;
    private Context mContext;
    private static SharedPreferences mAppPrefs;

    protected SharedPreferenceHelper(Context ctx) {
        mContext = ctx;
        mAppPrefs = mContext.getSharedPreferences(APP_PREFERENCES,mContext.MODE_PRIVATE);
    }

    public static SharedPreferenceHelper getInstance(Context ctx) {
        if(instance == null) {
            instance = new SharedPreferenceHelper(ctx);
        }
        return instance;
    }

    public void saveFirstRunAppToPref() {
       SharedPreferences.Editor editor =  mAppPrefs.edit();
       editor.putBoolean(FIRST_RUN,false);
       editor.commit();
    }

    public boolean getFirstRunAppValue() {
        return  mAppPrefs.getBoolean(FIRST_RUN,true);
    }

    private void saveIsPasscodedValue(boolean value) {
        SharedPreferences.Editor editor =  mAppPrefs.edit();
        editor.putBoolean(IS_PASSCODED,value);
        editor.commit();
    }

    private boolean getIsPasscodedValue() {
        return  mAppPrefs.getBoolean(IS_PASSCODED,true);
    }

    public void saveActionList(ActionDataSource dataSource) {
        SharedPreferences.Editor editor =  mAppPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(dataSource);
        editor.putString("SerializableObject", json);
        editor.commit();
    }

    public ActionDataSource getAlarmActionList() {

        Gson gson = new Gson();
        String json = mAppPrefs.getString("SerializableObject", "");
        return gson.fromJson(json, ActionDataSource.class);
    }


}
