package antitheftalarm.hanhnv.com.antitheftalarm.utils;

import android.content.Context;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

public class DisplayInfo {
    private float screen_height=0;
    private float screen_width=0;
    private int screen_height_pixel = 0;
    private int screen_width_pixel = 0;
    private WindowManager wm;
    private DisplayMetrics displaymetrics;


    private float density;


    public DisplayInfo(Context context) {
        getDisplayheightWidth(context);
    }

     void getDisplayheightWidth(Context context) {
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        displaymetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        screen_height_pixel = displaymetrics.heightPixels;
        screen_width_pixel = displaymetrics.widthPixels;

        density = displaymetrics.density;
        screen_height = screen_height_pixel / density;
        screen_width = screen_width_pixel / density;
    }


    public float getScreen_height() {
        return screen_height;
    }

    public float getScreen_width() {
        return screen_width;
    }

    public int getScreen_height_pixel() {
        return screen_height_pixel;
    }

    public int getScreen_width_pixel() {
        return screen_width_pixel;
    }

    public float getDensity() {
        return density;
    }


}