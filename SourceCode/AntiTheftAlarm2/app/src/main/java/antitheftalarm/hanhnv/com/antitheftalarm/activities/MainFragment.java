package antitheftalarm.hanhnv.com.antitheftalarm.activities;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import antitheftalarm.hanhnv.com.antitheftalarm.R;
import antitheftalarm.hanhnv.com.antitheftalarm.datasources.ActionStatus;
import antitheftalarm.hanhnv.com.antitheftalarm.datasources.ActionType;
import antitheftalarm.hanhnv.com.antitheftalarm.datasources.AlarmAction;

/**
 * Created by Admin on 2/23/2018.
 */

public class MainFragment extends Fragment implements SwitchCompat.OnCheckedChangeListener {
    private static final String ARG_SECTION_NUMBER = "section_number";

    ConstraintLayout chargerView;
    TextView chargerMainText;
    TextView chargerSubText;
    SwitchCompat chargerSwitch;
    ImageView chargerImage;

    ConstraintLayout motionView;
    TextView motionMainText;
    TextView motionSubText;
    SwitchCompat motionSwitch;
    ImageView motionImage;

    ConstraintLayout proximityView;
    TextView proximityMainText;
    TextView proximitySubText;
    SwitchCompat proximitySwitch;
    ImageView proximityImage;

    ConstraintLayout simcardView;
    TextView simcardMainText;
    TextView simcardSubText;
    SwitchCompat simcardSwitch;
    ImageView simcardImage;

    private ArrayList<AlarmAction> alarmActions;


    public MainFragment() {


    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    /**
     *
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        public AlarmAction(ActionType actionType, String actionName, ActionStatus actionStatus, String actionDescription, boolean isAutomatically, double timeAutomation, String imageName) {
//            this.actionType = actionType;
//            this.actionName = actionName;
//            this.actionStatus = actionStatus;
//            this.actionDescription = actionDescription;
//            this.isAutomatically = isAutomatically;
//            this.timeAutomation = timeAutomation;
//            this.imageName = imageName;
//        }

        if (alarmActions == null) {
            alarmActions = new ArrayList<>();
            alarmActions.add(new AlarmAction(ActionType.CHARGER, getResources().getString(R.string.charger_main_text),ActionStatus.DISABLED,getResources().getString(R.string.charger_sub_text),false,0.0,"charger.png"));
            alarmActions.add(new AlarmAction(ActionType.MOTION, getResources().getString(R.string.motion_main_text),ActionStatus.ENABLED,getResources().getString(R.string.motion_sub_text),false,0.0,"motion.png"));
            alarmActions.add(new AlarmAction(ActionType.PROXIMITY, getResources().getString(R.string.proximity_main_text),ActionStatus.DISABLED,getResources().getString(R.string.proximity_sub_text),false,0.0,"proximity.png"));
            alarmActions.add(new AlarmAction(ActionType.SIMCARD, getResources().getString(R.string.simcard_main_text),ActionStatus.ENABLED,getResources().getString(R.string.simcard_sub_text),false,0.0,"simcard.png"));
        }

        prepareViews();
        setupViews();

    }

    public void prepareViews() {

        chargerView = getView().findViewById(R.id.chargerView);
        chargerMainText = getView().findViewById(R.id.chargerModeTitle);
        chargerSubText = getView().findViewById(R.id.chargerDescription);
        chargerSwitch = getView().findViewById(R.id.chargerSwitch);
        chargerImage = getView().findViewById(R.id.chargerImage);

        //chargerSwitch.setThumbTintList(myList);
        //chargerSwitch.setTrackTintList(myList2);
        motionView = getView().findViewById(R.id.motionView);
        motionMainText = getView().findViewById(R.id.motionModeTitle);
        motionSubText = getView().findViewById(R.id.motionDescription);
        motionSwitch = getView().findViewById(R.id.motionSwitch);
        motionImage = getView().findViewById(R.id.motionImage);

        proximityView = getView().findViewById(R.id.proximityView);
        proximityMainText = getView().findViewById(R.id.proximityModeTitle);
        proximitySubText = getView().findViewById(R.id.proximityDescription);
        proximitySwitch = getView().findViewById(R.id.proximitySwitch);
        proximityImage = getView().findViewById(R.id.proximityImage);

        simcardView = getView().findViewById(R.id.simcardView);
        simcardMainText = getView().findViewById(R.id.simcardModeTitle);
        simcardSubText = getView().findViewById(R.id.simcardDescription);
        simcardSwitch = getView().findViewById(R.id.simcardSwitch);
        simcardImage = getView().findViewById(R.id.simcardImage);

        chargerSwitch.setOnCheckedChangeListener(this);
        motionSwitch.setOnCheckedChangeListener(this);
        proximitySwitch.setOnCheckedChangeListener(this);
        simcardSwitch.setOnCheckedChangeListener(this);




    }


    private void setupViews() {

        for (AlarmAction action : alarmActions) {
            switch (action.getActionType()) {
                case CHARGER:

                    chargerMainText.setText(action.getActionName());
                    chargerSubText.setText(action.getActionDescription());

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        chargerSwitch.setChecked(true);
                    } else {
                        chargerSwitch.setChecked(false);
                    }

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        chargerMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                        chargerSubText.setTextColor(getResources().getColor(R.color.subTextOn));
                        chargerImage.setImageResource(R.drawable.charger_unhighlighted);
                        TransitionDrawable transition = (TransitionDrawable) chargerView.getBackground();
                        transition.startTransition(150);
                    } else {
                        chargerMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                        chargerSubText.setTextColor(getResources().getColor(R.color.subTextOff));
                        chargerImage.setImageResource(R.drawable.charger_highlighted);
                        TransitionDrawable transition = (TransitionDrawable) chargerView.getBackground();
                        transition.startTransition(1);
                        transition.reverseTransition(150);
                    }

                    break;
                case MOTION:
                    motionMainText.setText(action.getActionName());
                    motionSubText.setText(action.getActionDescription());

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        motionSwitch.setChecked(true);
                    } else {
                        motionSwitch.setChecked(false);
                    }

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        motionMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                        motionSubText.setTextColor(getResources().getColor(R.color.subTextOn));
                        motionImage.setImageResource(R.drawable.motion_unhighlighted);
                        TransitionDrawable transition = (TransitionDrawable) motionView.getBackground();
                        transition.startTransition(150);
                    } else {
                        motionMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                        motionSubText.setTextColor(getResources().getColor(R.color.subTextOff));
                        motionImage.setImageResource(R.drawable.motion_highlighted);
                        TransitionDrawable transition = (TransitionDrawable) motionView.getBackground();
                        transition.startTransition(1);
                        transition.reverseTransition(150);
                    }
                    break;
                case PROXIMITY:
                    proximityMainText.setText(action.getActionName());
                    proximitySubText.setText(action.getActionDescription());

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        proximitySwitch.setChecked(true);
                    } else {
                        proximitySwitch.setChecked(false);
                    }

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        proximityMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                        proximitySubText.setTextColor(getResources().getColor(R.color.subTextOn));
                        proximityImage.setImageResource(R.drawable.proximity_unhightlighted);
                        TransitionDrawable transition = (TransitionDrawable) proximityView.getBackground();
                        transition.startTransition(150);
                    } else {
                        proximityMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                        proximitySubText.setTextColor(getResources().getColor(R.color.subTextOff));
                        proximityImage.setImageResource(R.drawable.proximity_hightlighted);
                        TransitionDrawable transition = (TransitionDrawable) proximityView.getBackground();
                        transition.startTransition(1);
                        transition.reverseTransition(150);
                    }
                    break;
                case SIMCARD:
                    simcardMainText.setText(action.getActionName());
                    simcardSubText.setText(action.getActionDescription());

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        simcardSwitch.setChecked(true);
                    } else {
                        simcardSwitch.setChecked(false);
                    }

                    if (action.getActionStatus() == ActionStatus.ENABLED) {
                        simcardMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                        simcardSubText.setTextColor(getResources().getColor(R.color.subTextOn));
                        simcardImage.setImageResource(R.drawable.simcard_unhightlighted);

                        TransitionDrawable transition = (TransitionDrawable) simcardView.getBackground();
                        transition.startTransition(150);
                    } else {
                        simcardMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                        simcardSubText.setTextColor(getResources().getColor(R.color.subTextOff));
                        simcardImage.setImageResource(R.drawable.simcard_hightlighted);
                        TransitionDrawable transition = (TransitionDrawable) simcardView.getBackground();
                        transition.startTransition(1);
                        transition.reverseTransition(150);
                    }
                    break;
                default:
                    break;
            }
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.chargerSwitch:
                changeCardStatus(ActionType.CHARGER,b);
                break;
            case R.id.motionSwitch:
                changeCardStatus(ActionType.MOTION,b);
                break;
            case R.id.proximitySwitch:
                changeCardStatus(ActionType.PROXIMITY,b);
                break;
            case R.id.simcardSwitch:
                changeCardStatus(ActionType.SIMCARD,b);
                break;

                default:
                    break;
        }
    }


    private void changeCardStatus(ActionType actionType, boolean activated) {
        switch (actionType) {
            case CHARGER:

                if (activated) {
                    chargerMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                    chargerSubText.setTextColor(getResources().getColor(R.color.subTextOn));
                    chargerImage.setImageResource(R.drawable.charger_unhighlighted);
                    TransitionDrawable transition = (TransitionDrawable) chargerView.getBackground();
                    transition.startTransition(150);
                } else {
                    chargerMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                    chargerSubText.setTextColor(getResources().getColor(R.color.subTextOff));
                    chargerImage.setImageResource(R.drawable.charger_highlighted);
                    //chargerView.setBackground(getResources().getDrawable(R.drawable.card_gradient_off));
                    TransitionDrawable transition = (TransitionDrawable) chargerView.getBackground();
                    transition.reverseTransition(150);
                }

                break;
            case MOTION:

                if (activated) {
                    motionMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                    motionSubText.setTextColor(getResources().getColor(R.color.subTextOn));
                    motionImage.setImageResource(R.drawable.motion_unhighlighted);

                    TransitionDrawable transition = (TransitionDrawable) motionView.getBackground();
                    transition.startTransition(150);
                } else {
                    motionMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                    motionSubText.setTextColor(getResources().getColor(R.color.subTextOff));
                    motionImage.setImageResource(R.drawable.motion_highlighted);

                    TransitionDrawable transition = (TransitionDrawable) motionView.getBackground();
                    transition.reverseTransition(150);
                }
                break;
            case PROXIMITY:


                if (activated) {
                    proximityMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                    proximitySubText.setTextColor(getResources().getColor(R.color.subTextOn));
                    proximityImage.setImageResource(R.drawable.proximity_unhightlighted);

                    TransitionDrawable transition = (TransitionDrawable) proximityView.getBackground();
                    transition.startTransition(150);
                } else {
                    proximityMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                    proximitySubText.setTextColor(getResources().getColor(R.color.subTextOff));
                    proximityImage.setImageResource(R.drawable.proximity_hightlighted);

                    TransitionDrawable transition = (TransitionDrawable) proximityView.getBackground();
                    transition.reverseTransition(150);
                }
                break;
            case SIMCARD:


                if (activated) {
                    simcardMainText.setTextColor(getResources().getColor(R.color.mainTextOn));
                    simcardSubText.setTextColor(getResources().getColor(R.color.subTextOn));
                    simcardImage.setImageResource(R.drawable.simcard_unhightlighted);

                    TransitionDrawable transition = (TransitionDrawable) simcardView.getBackground();
                    transition.startTransition(150);
                } else {
                    simcardMainText.setTextColor(getResources().getColor(R.color.mainTextOff));
                    simcardSubText.setTextColor(getResources().getColor(R.color.subTextOff));
                    simcardImage.setImageResource(R.drawable.simcard_hightlighted);
                    TransitionDrawable transition = (TransitionDrawable) simcardView.getBackground();
                    transition.reverseTransition(150);
                }
                break;
            default:
                break;
        }
    }
}
