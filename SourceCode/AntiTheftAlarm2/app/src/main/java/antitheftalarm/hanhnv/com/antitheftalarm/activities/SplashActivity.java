package antitheftalarm.hanhnv.com.antitheftalarm.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;

import antitheftalarm.hanhnv.com.antitheftalarm.R;
import antitheftalarm.hanhnv.com.antitheftalarm.activities.MainActivity;

public class SplashActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {


    private final int SPLASH_DISPLAY_LENGTH = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();


        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent activityIntent = new Intent(SplashActivity.this, MainActivity.class);
                SplashActivity.this.startActivity(activityIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

//        TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
////        String number = tm.getLine1Number();
//
//
//        // Here, thisActivity is the current activity
//        if (ContextCompat.checkSelfPermission(SplashActivity.this,
//                Manifest.permission.READ_PHONE_STATE)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this,
//                    Manifest.permission.READ_PHONE_STATE)) {
//
//                // Show an explanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//
//            } else {
//
//                // No explanation needed, we can request the permission.
//
//
//
//                ActivityCompat.requestPermissions(SplashActivity.this,new String[]{Manifest.permission.READ_PHONE_STATE},1);
//                System.out.print("HHH: request permission");
//
//                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                // app-defined int constant. The callback method gets the
//                // result of the request.
//            }
//        } else {
//            // Permission has already been granted
//
//            System.out.print("HHH: permission has been granted ");
//
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    System.out.println("HHH: phone state granted");
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
                    //System.out.println("HHH: phone state granted" + tm.getLine1Number());


                } else {

                    System.out.println("HHH: phone state permissiton denied");
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    public void onClick(View view) {
        Intent activityIntent = new Intent(this, MainActivity.class);
        this.startActivity(activityIntent);
    }
}
