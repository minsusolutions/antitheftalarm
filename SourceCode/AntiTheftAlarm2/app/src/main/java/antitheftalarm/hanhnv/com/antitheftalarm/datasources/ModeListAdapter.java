package antitheftalarm.hanhnv.com.antitheftalarm.datasources;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import antitheftalarm.hanhnv.com.antitheftalarm.R;
import antitheftalarm.hanhnv.com.antitheftalarm.activities.MainActivity;
import antitheftalarm.hanhnv.com.antitheftalarm.utils.DisplayInfo;

public class ModeListAdapter extends BaseAdapter {


    private List<Mode> listData;
    private LayoutInflater layoutInflater;
    private Context context;
    private int displayHeight;
    private int displayWidth;
    private DisplayInfo displayInfo;
    public static final String TAG = "MODE_LIST_ADAPTER";

    public ModeListAdapter(Context aContext, List<Mode> listData) {
        context = aContext;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
        displayInfo = new DisplayInfo(aContext);
    }


    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_mode_item, null);

            ViewGroup.LayoutParams params = convertView.getLayoutParams();
            if (params == null) {
                params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            }

            params.width = (int)(displayInfo.getScreen_width_pixel() - 10 * 2 * displayInfo.getDensity() - 10 * displayInfo.getDensity())/2;
            params.height = (int)(60 * displayInfo.getDensity());

            Log.i(TAG,"item width" + String.valueOf(params.width));
            Log.i(TAG,"item height" + String.valueOf(params.height));

            convertView.setLayoutParams(params);
            holder = new ViewHolder();
            holder.modeImage = (ImageView) convertView.findViewById(R.id.imageView_mode);
            holder.modeName = (TextView) convertView.findViewById(R.id.textView_modeName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Mode mode = this.listData.get(position);
        holder.modeName.setText(mode.getModeName());

        int imageId = this.getMipmapResIdByName(mode.getImageName());

        holder.modeImage.setImageResource(imageId);

        convertView.requestLayout();

        return convertView;
    }

    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }

    static class ViewHolder {
        ImageView modeImage;
        TextView modeName;
    }

    public int getMipmapResIdByName(String resName)  {
        String pkgName = context.getPackageName();

        int resID = context.getResources().getIdentifier(resName , "drawable", pkgName);
        return resID;
    }

}

