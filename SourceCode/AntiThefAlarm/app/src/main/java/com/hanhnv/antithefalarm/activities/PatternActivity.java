package com.hanhnv.antithefalarm.activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;


import com.eftimoff.patternview.PatternView;
import com.hanhnv.antithefalarm.R;
import com.hanhnv.antithefalarm.utils.ConstantValues;
import com.hanhnv.antithefalarm.utils.SettingsContentObserver;

public class PatternActivity extends AppCompatActivity {

    private PatternView mPatternView;
    private TextView mTextview;
    private String mPatternString;

    private boolean mConfirmPattern;
    private String mTempPattern;
    private int mPatternType;

    private SharedPreferences mAppPrefs;
    private SharedPreferences.Editor mPrefEditor;

    private Vibrator mVibrator;
    private MediaPlayer mPlayer;
    private CountDownTimer mCountDownTimer;
    private static Camera cam = null;
    private Thread mFlashLightThread;
    private boolean mStopFlashLight = false;

    private boolean mObserverActivated = false;

    SettingsContentObserver mSettingsContentObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pattern);




        mPatternView = (PatternView) findViewById(R.id.patternView);
        mTextview = (TextView) findViewById(R.id.textView);
        mAppPrefs = getSharedPreferences(ConstantValues.APP_PREFERENCES,MODE_PRIVATE);

        mPatternType = getIntent().getIntExtra(ConstantValues.PASSWORD_MODE_TYPE,ConstantValues.PASSWORD_MODE_INI);

        if(mPatternType == ConstantValues.PASSWORD_MODE_REQUIRED) {
            mTextview.setText(R.string.title_enter_your_pattern);
            graceAlarm();
        } else {
            mTextview.setText(R.string.title_enter_your_pattern);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mPatternView.setOnPatternDetectedListener(new PatternView.OnPatternDetectedListener() {

            @Override
            public void onPatternDetected() {
                if(mPatternType == ConstantValues.PASSWORD_MODE_INI) {
                    if(!mConfirmPattern) {
                        mTempPattern = mPatternView.getPatternString();
                        mTextview.setText(R.string.title_enter_confirm_pattern);
                        mPatternView.clearPattern();
                        mConfirmPattern=true;
                    } else {
                        if(mTempPattern.equalsIgnoreCase(mPatternView.getPatternString())) {
                            mPrefEditor = mAppPrefs.edit();
                            mPrefEditor.putString(ConstantValues.PATTERN, mTempPattern);
                            mPrefEditor.putInt(ConstantValues.PASSWORD_TYPE, ConstantValues.PASSWORD_TYPE_PATTERN);
                            mPrefEditor.commit();
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.title_passcode_notmactch, Toast.LENGTH_SHORT).show();
                            vibrate(-1);
                        }
                    }
                } else if (mPatternType == ConstantValues.PASSWORD_MODE_REQUIRED) {
                    mPatternString = mAppPrefs.getString(ConstantValues.PATTERN,null);
                    if(mPatternString.equalsIgnoreCase(mPatternView.getPatternString())) {
                        cancelNotifications();
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.title_passcode_notmactch, Toast.LENGTH_SHORT).show();
                    }
                } else if(mPatternType == ConstantValues.PASSWORD_MODE_CHANGE) {
                    mPatternString = mAppPrefs.getString(ConstantValues.PATTERN,null);
                    if(mPatternString.equalsIgnoreCase(mPatternView.getPatternString())) {
                        mPatternType = ConstantValues.PASSWORD_MODE_INI;
                        mConfirmPattern = false;
                        mPatternView.clearPattern();
                        mTextview.setText(R.string.title_enter_your_new_pattern);
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.title_passcode_notmactch, Toast.LENGTH_SHORT).show();
                        vibrate(-1);
                    }

                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void vibrate(int loop) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isVibrate = prefs.getBoolean("notifications_vibrate",true);
        if (isVibrate) {
            if (mVibrator == null)
                mVibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
            mVibrator.cancel();
            long pattern[] = {0, 200, 0};
            mVibrator.vibrate(pattern, loop);
        }
    }

    private void playSound(boolean isLooping) {
        stopPlayingSound();
        enableMediaNotifications();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String soundIndex = prefs.getString("alarm_tone", "1");
        switch (soundIndex) {
            case "1":
                mPlayer = MediaPlayer.create(this, R.raw.car_horn);
                break;
            case "2":
                mPlayer = MediaPlayer.create(this,R.raw.car_horn);
                break;
            case "3":
                mPlayer = MediaPlayer.create(this,R.raw.fire_alarm);
                break;
            case "4":
                mPlayer = MediaPlayer.create(this,R.raw.siren);
                break;
            case "5":
                mPlayer = MediaPlayer.create(this,R.raw.scary_sound);
                break;
        }
        mPlayer.setLooping(isLooping);
        mPlayer.start();
    }

    private void stopPlayingSound() {
        if(mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }

        if(mSettingsContentObserver != null && mObserverActivated) {
            getApplicationContext().getContentResolver().unregisterContentObserver(mSettingsContentObserver);
            mObserverActivated = false;
        }
    }

    private void graceAlarm() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String grace = prefs.getString("grace","1");

        switch (grace) {
            case "1":
                countDownToAlarm(0);
                break;
            case "2":
                countDownToAlarm(5000);
                break;
            case "3":
                countDownToAlarm(10000);
                break;
            case "4":
                countDownToAlarm(15000);
                break;
            default:
                break;

        }
    }
    private void countDownToAlarm(long l) {
        mCountDownTimer = new CountDownTimer(l, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                playSound(true);
                vibrate(0);
                flashLight();
            }
        };
        mCountDownTimer.start();
    }

    private void cancelNotifications() {
        if(mCountDownTimer != null)
            mCountDownTimer.cancel();
        if(mVibrator != null)
            mVibrator.cancel();
        stopPlayingSound();
        mStopFlashLight = true;
    }

    private void flashLight() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean flashLight = prefs.getBoolean("notifications_flashlight",true);
        if(flashLight) {
         mFlashLightThread =   new Thread (new Runnable() {
                long blinkDelay = 30; //Delay in ms
                int i=0;
                @Override
                public void run() {
                    mStopFlashLight = false;

                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (currentapiVersion < Build.VERSION_CODES.M){
                        while (true) {
                            if(mStopFlashLight) {
                                flashLightOff();
                                break;
                            }
                            if (i % 2 == 0) {
                                flashLightOn();
                            } else {
                                flashLightOff();
                            }
                            try {
                                Thread.sleep(blinkDelay);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            i++;
                        }
                    } else{
                        while (true) {
                            if(mStopFlashLight) {
                                PatternActivity.handleActionTurnOffFlashLight(getApplicationContext());
                                break;
                            }
                            if (i % 2 == 0) {
                                PatternActivity.handleActionTurnOnFlashLight(getApplicationContext());
                            } else {
                                PatternActivity.handleActionTurnOffFlashLight(getApplicationContext());
                            }
                            try {
                                Thread.sleep(blinkDelay);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            i++;
                        }
                    }
                }
            });
            mFlashLightThread.start();
        }
    }

    public void flashLightOn() {

            try {
                if (getPackageManager().hasSystemFeature(
                        PackageManager.FEATURE_CAMERA_FLASH) && cam==null) {
                    cam = Camera.open();
                    Parameters p = cam.getParameters();
                    p.setFlashMode(Parameters.FLASH_MODE_TORCH);
                    cam.setParameters(p);
                    cam.startPreview();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    public void flashLightOff() {
            try {
                if (getPackageManager().hasSystemFeature(
                        PackageManager.FEATURE_CAMERA_FLASH) && cam != null) {
                    cam.stopPreview();
                    cam.release();
                    cam = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    @Override
    public void onBackPressed() {
        if(mPatternType != ConstantValues.PASSWORD_MODE_REQUIRED)
        {
            super.onBackPressed();
        }
        else {
            Toast.makeText(getApplicationContext(),"You cannot go back on this stage",Toast.LENGTH_LONG).show();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private static void handleActionTurnOnFlashLight(Context context){
        try{

            CameraManager manager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
            String[] list = manager.getCameraIdList();
            manager.setTorchMode(list[0], true);
        }
        catch (CameraAccessException cae){
            //Log.e(TAG, cae.getMessage());
            cae.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private static void handleActionTurnOffFlashLight(Context context){
        try{
            CameraManager manager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
            manager.setTorchMode(manager.getCameraIdList()[0], false);
        }
        catch (CameraAccessException cae){
            //Log.e(TAG, cae.getMessage());
            cae.printStackTrace();
        }
    }

    private void enableMediaNotifications() {
        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int maxVolume = audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        audio.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);

        mSettingsContentObserver = new SettingsContentObserver(this,new Handler());
        getApplicationContext().getContentResolver().registerContentObserver(android.provider.Settings.System.CONTENT_URI, true, mSettingsContentObserver);
        mObserverActivated = true;
    }

}
