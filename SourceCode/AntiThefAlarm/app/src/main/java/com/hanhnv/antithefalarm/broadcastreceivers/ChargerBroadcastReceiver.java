package com.hanhnv.antithefalarm.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by hanh on 6/10/2016.
 */
public class ChargerBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        if(action.equals(Intent.ACTION_POWER_CONNECTED)) {
            Toast.makeText(context,"Charger Plugged",Toast.LENGTH_LONG).show();
        }
        else if(action.equals(Intent.ACTION_POWER_DISCONNECTED)) {
            Toast.makeText(context,"Charger Unplugged",Toast.LENGTH_LONG).show();
        }
    }
}
