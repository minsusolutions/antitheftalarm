package com.hanhnv.antithefalarm.utils;

/**
 * Created by sev_user on 3/25/2016.
 */
public class ConstantValues {

    public static final String APP_PREFERENCES = "APP_PREFERENCES";

    public static final String PASSWORD_TYPE = "PASSWORD_TYPE";
    public static final int PASSWORD_TYPE_NONE = 0;
    public static final int PASSWORD_TYPE_PASSCODE = 1;
    public static final int PASSWORD_TYPE_PATTERN = 2;

    public static final String PASSCODE = "PASSCODE";
    public static final String PATTERN = "PATTERN";
    public static final String PASSWORD_MODE_TYPE = "PASSWORD_MODE_TYPE";

    public static final int PASSWORD_MODE_INI = 0;
    public static final int PASSWORD_MODE_REQUIRED = 1;
    public static final int PASSWORD_MODE_CHANGE = 2;


    public static final String ACTION_PASSWORD_MODE_REQUIRE = "action.minsu.com.password.require";


}
