package com.hanhnv.antithefalarm.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

public class SimcardChangeBroadcaseReceiver extends BroadcastReceiver {
    public SimcardChangeBroadcaseReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        TelephonyManager telephoneMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telephoneMgr.getSimState();
        String sim = Integer.toString(simState);

        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                System.out.println("*******************************************Sim State absent******************************");
                Log.i("SimStateListener", "Sim State absent");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                System.out.println("*******************************************SIM_STATE_NETWORK_LOCKED******************************" + simState);
                Log.i("SimStateListener", "Sim State network locked");
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                System.out.println("*******************************************SIM_STATE_PIN_REQUIRED******************************" + simState);
                Log.i("SimStateListener", "Sim State pin required");
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                System.out.println("*******************************************SIM_STATE_PUK_REQUIRED******************************" + simState);
                Log.i("SimStateListener", "Sim State puk required");
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                System.out.println("*******************************************SIM_STATE_UNKNOWN******************************" + simState);
                Log.i("SimStateListener", "Sim State unknown");
                break;
        }
    }
}
