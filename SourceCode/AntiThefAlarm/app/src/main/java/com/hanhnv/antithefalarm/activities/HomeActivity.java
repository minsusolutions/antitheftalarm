package com.hanhnv.antithefalarm.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.hanhnv.antithefalarm.R;
import com.hanhnv.antithefalarm.broadcastreceivers.ChargerBroadcastReceiver;
import com.hanhnv.antithefalarm.utils.ConstantValues;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SensorEventListener {

    RelativeLayout mChargerLayout;
    RelativeLayout mMotionLayout;
    RelativeLayout mProximityLayout;
    RelativeLayout mSimcardLayout;

    ShimmerTextView mChargerActivatedShimmer;
    ShimmerTextView mMotionActivatedShimmer;
    ShimmerTextView mProximityActivatedShimmer;
    ShimmerTextView mSimcardActivatedShimmer;

    TextView mChargerTitleTextView;
    TextView mChargerDescriptionTextView;
    TextView mChargerDeactivateTextView;

    TextView mMotionTitleTextView;
    TextView mMotionDescriptionTextView;
    TextView mMotionDeactivateTextView;

    TextView mProximityTitleTextView;
    TextView mProximityDescriptionTextView;
    TextView mProximityDeactivateTextView;

    TextView mSimcardTitleTextView;
    TextView mSimcardDescriptionTextView;
    TextView mSimcardDeactivateTextView;

    MaterialDialog materialDialog;
    TextView remainingTime;

    CountDownTimer mCountDownTimer;

    float mAccel = 0;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mProximity;

    SharedPreferences mAppPrefs;

    public boolean isChargerRegistered;
    public boolean isMotionRegistered;
    public boolean isProxiRegistered;
    public boolean isSimcardRegisted;
    public boolean isOnPasscodeRequire;

    public boolean isChargerActivated;
    public boolean isMotionActivated;
    public boolean isProximityActivated;
    public boolean isSimcardActivated;

    private String mSensorLevel;
    AdView mAdView;

    BroadcastReceiver mChargerBR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        initView();
        setOnclickRelaytivesLayout();
        setAllFlagRegistedToFalse();
        utils();

        mChargerBR = new ChargerBroadcastReceiver();



    }

    private void initView() {
        mChargerLayout = (RelativeLayout) findViewById(R.id.charger_layout);
        mMotionLayout = (RelativeLayout) findViewById(R.id.motion_layout);
        mProximityLayout = (RelativeLayout) findViewById(R.id.proximi_layout);
        mSimcardLayout = (RelativeLayout) findViewById(R.id.simcard_layout);

        mChargerActivatedShimmer = (ShimmerTextView) findViewById(R.id.charger_activate);
        mMotionActivatedShimmer = (ShimmerTextView) findViewById(R.id.motion_activate);
        mProximityActivatedShimmer = (ShimmerTextView) findViewById(R.id.proximity_activate);
        mSimcardActivatedShimmer = (ShimmerTextView) findViewById(R.id.simcard_activate);

        mChargerTitleTextView = (TextView) findViewById(R.id.titleCharger);
        mChargerDescriptionTextView = (TextView) findViewById(R.id.description_charger);
        mChargerDeactivateTextView = (TextView) findViewById(R.id.deactive_charger);

        mMotionTitleTextView = (TextView) findViewById(R.id.titleMotion);
        mMotionDescriptionTextView = (TextView) findViewById(R.id.description_motion);
        mMotionDeactivateTextView = (TextView) findViewById(R.id.deactive_motion);

        mProximityTitleTextView = (TextView) findViewById(R.id.titleProximi);
        mProximityDescriptionTextView = (TextView) findViewById(R.id.description_proximi);
        mProximityDeactivateTextView = (TextView) findViewById(R.id.deactive_proximity);

        mSimcardTitleTextView = (TextView) findViewById(R.id.titleSimcard);
        mSimcardDescriptionTextView = (TextView) findViewById(R.id.description_simcard);
        mSimcardDeactivateTextView = (TextView) findViewById(R.id.deactive_simcard);

        remainingTime = (TextView) findViewById(R.id.remainingTime);

        setChargerDeactivateView();
        setMotionDeactivateView();
        setProximityDeactivateView();
        setSimcardDeactivateView();

        Shimmer shimmer = new Shimmer();
        shimmer.start(mChargerActivatedShimmer);
        shimmer.start(mMotionActivatedShimmer);
        shimmer.start(mProximityActivatedShimmer);
        shimmer.start(mSimcardActivatedShimmer);
    }

    private void setOnclickRelaytivesLayout() {

        mChargerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkPasswordSetup()) {
                    showDialogGotoSetting();
                } else {
                    if(!isChargerActivated) {
                        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_POWER_CONNECTED);
                        IntentFilter intentFilter1 = new IntentFilter(Intent.ACTION_POWER_DISCONNECTED);
                        registerReceiver(mChargerBR, intentFilter);
                        registerReceiver(mChargerBR, intentFilter1);
                        isChargerActivated = true;
                        setChargerActiveView();
                    } else {
                        cancelAllRegister();
                        setAllFlagRegistedToFalse();
                        alarm();
                    }
                }
            }
        });

        mMotionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkPasswordSetup()) {
                    showDialogGotoSetting();
                } else {
                    if(!isMotionActivated) {
                        boolean wrapInScrollView = true;
                        materialDialog = new MaterialDialog.Builder(HomeActivity.this)
                                .customView(R.layout.alert_dialog_motion,wrapInScrollView)
                                .cancelable(false)
                                .show();
                        remainingTime = (TextView)materialDialog.getCustomView().findViewById(R.id.remainingTime);
                        mCountDownTimer = new CountDownTimer(10000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                if(remainingTime != null)
                                    remainingTime.setText("" + (millisUntilFinished / 1000));
                            }

                            @Override
                            public void onFinish() {
                                if(remainingTime != null)
                                    remainingTime.setText("0");
                                mSensorManager.registerListener(HomeActivity.this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
                                isMotionActivated = true;
                                materialDialog.dismiss();
                                setMotionActiveView();
                            }
                        };
                        mCountDownTimer.start();
                    } else {
                        cancelAllRegister();
                        setAllFlagRegistedToFalse();
                        alarm();
                    }
                }
            }
        });

        mProximityLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkPasswordSetup()) {
                    showDialogGotoSetting();
                } else {
                    if(!isProximityActivated) {

                        boolean wrapInScrollView = true;
                        materialDialog = new MaterialDialog.Builder(HomeActivity.this)
                                .customView(R.layout.alert_dialog_proximity,wrapInScrollView)
                                .cancelable(false)
                                .show();
                        ShimmerTextView shimmertv = (ShimmerTextView)materialDialog.getCustomView().findViewById(R.id.dialog_proximi_shimmer);
                        Shimmer shimmer = new Shimmer();
                        shimmer.start(shimmertv);
                        remainingTime = (TextView)materialDialog.getCustomView().findViewById(R.id.remainingTime);

                        mCountDownTimer = new CountDownTimer(10000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                if(remainingTime != null)
                                    remainingTime.setText("" + (millisUntilFinished / 1000));
                            }

                            @Override
                            public void onFinish() {
                                if(remainingTime != null)
                                    remainingTime.setText("0");
                                mSensorManager.registerListener(HomeActivity.this, mProximity, SensorManager.SENSOR_DELAY_NORMAL);
                                materialDialog.dismiss();
                                isProximityActivated = true;
                                setProximityActiveView();
                            }
                        };
                        mCountDownTimer.start();
                    } else {
                        cancelAllRegister();
                        setAllFlagRegistedToFalse();
                        alarm();
                    }
                }
            }
        });

        mSimcardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkPasswordSetup()) {
                    showDialogGotoSetting();
                } else {
                    if(!isSimcardActivated) {
                        IntentFilter simcardIntentFilter = new IntentFilter("android.intent.action.SIM_STATE_CHANGED");
                        registerReceiver(mSimcardRemoval, simcardIntentFilter);
                        setSimcardActiveView();
                        isSimcardActivated = true;
                    } else {
                        cancelAllRegister();
                        setAllFlagRegistedToFalse();
                        alarm();
                    }
                }
            }
        });
    }

    private void setChargerActiveView() {
        mChargerTitleTextView.setVisibility(View.INVISIBLE);
        mChargerDescriptionTextView.setVisibility(View.INVISIBLE);
        mChargerActivatedShimmer.setVisibility(View.VISIBLE);
        mChargerDeactivateTextView.setVisibility(View.VISIBLE);
    }

    private void setChargerDeactivateView() {
        mChargerTitleTextView.setVisibility(View.VISIBLE);
        mChargerDescriptionTextView.setVisibility(View.VISIBLE);
        mChargerActivatedShimmer.setVisibility(View.INVISIBLE);
        mChargerDeactivateTextView.setVisibility(View.INVISIBLE);
    }

    private void setMotionActiveView() {
        mMotionTitleTextView.setVisibility(View.INVISIBLE);
        mMotionDescriptionTextView.setVisibility(View.INVISIBLE);
        mMotionActivatedShimmer.setVisibility(View.VISIBLE);
        mMotionDeactivateTextView.setVisibility(View.VISIBLE);
    }

    private void setMotionDeactivateView() {
        mMotionTitleTextView.setVisibility(View.VISIBLE);
        mMotionDescriptionTextView.setVisibility(View.VISIBLE);
        mMotionActivatedShimmer.setVisibility(View.INVISIBLE);
        mMotionDeactivateTextView.setVisibility(View.INVISIBLE);
    }

    private void setProximityActiveView() {
        mProximityTitleTextView.setVisibility(View.INVISIBLE);
        mProximityDescriptionTextView.setVisibility(View.INVISIBLE);
        mProximityActivatedShimmer.setVisibility(View.VISIBLE);
        mProximityDeactivateTextView.setVisibility(View.VISIBLE);
    }

    private void setProximityDeactivateView() {
        mProximityTitleTextView.setVisibility(View.VISIBLE);
        mProximityDescriptionTextView.setVisibility(View.VISIBLE);
        mProximityActivatedShimmer.setVisibility(View.INVISIBLE);
        mProximityDeactivateTextView.setVisibility(View.INVISIBLE);
    }

    private void setSimcardActiveView() {
        mSimcardTitleTextView.setVisibility(View.INVISIBLE);
        mSimcardDescriptionTextView.setVisibility(View.INVISIBLE);
        mSimcardActivatedShimmer.setVisibility(View.VISIBLE);
        mSimcardDeactivateTextView.setVisibility(View.VISIBLE);
    }

    private void setSimcardDeactivateView() {
        mSimcardTitleTextView.setVisibility(View.VISIBLE);
        mSimcardDescriptionTextView.setVisibility(View.VISIBLE);
        mSimcardActivatedShimmer.setVisibility(View.INVISIBLE);
        mSimcardDeactivateTextView.setVisibility(View.INVISIBLE);
    }

    private void setAllFlagRegistedToFalse() {
        isChargerActivated = false;
        isMotionActivated = false;
        isProximityActivated = false;
        isSimcardActivated = false;
    }

    @Override
    protected void onResume() {

        if(mAdView == null) {
            mAdView = (AdView) findViewById(R.id.adView);
        }
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mSensorLevel = prefs.getString("sensor","1");

        if(isChargerActivated) {
            setChargerActiveView();
        } else {
            setChargerDeactivateView();
        }
        if(isMotionActivated) {
            setMotionActiveView();
        } else {
            setMotionDeactivateView();
        }
        if(isProximityActivated)
        {
            setProximityActiveView();
        } else {
            setProximityDeactivateView();
        }
        if(isSimcardActivated) {
            setSimcardActiveView();
        } else {
            setSimcardDeactivateView();
        }

        super.onResume();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (isChargerActivated || isMotionActivated || isProximityActivated || isSimcardActivated) {
                cancelAllRegister();
                setAllFlagRegistedToFalse();
                alarm();
            } else {

                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onDestroy() {
        cancelAllRegister();
        setAllFlagRegistedToFalse();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingIntent = new Intent(HomeActivity.this,SettingActivity.class);
            startActivity(settingIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_settings) {
            Intent settingIntent = new Intent(HomeActivity.this,SettingActivity.class);
            startActivity(settingIntent);
        }else if (id == R.id.nav_share) {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    getString(R.string.share_title) +": https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName());
            sendIntent.setType("text/plain");
            startActivity(sendIntent);

        } else if (id == R.id.nav_rate_us) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getApplicationContext().getPackageName()));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean checkPasswordSetup() {
        if(mAppPrefs == null) {
            mAppPrefs = getSharedPreferences(ConstantValues.APP_PREFERENCES,MODE_PRIVATE);
        }
        int passwordType = mAppPrefs.getInt(ConstantValues.PASSWORD_TYPE, ConstantValues.PASSWORD_TYPE_NONE);
        if(passwordType == ConstantValues.PASSWORD_TYPE_NONE)
            return false;
        return true;
    }
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            mostraValori(sensorEvent);
        }
        if(sensorEvent.sensor.getType() == Sensor.TYPE_PROXIMITY)
        {
            if (sensorEvent.values[0] == 0) {
                //near
                isProxiRegistered = true;

            } else {
                if(isProxiRegistered) {
                    isProxiRegistered = false;
                    cancelAllRegister();
                    setAllFlagRegistedToFalse();
                    alarm();
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    private void utils() {
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
    }

    private void mostraValori(SensorEvent event){



        float[] valori=event.values;//array che contiene i valori dell'accelerometro
        //modifica del valore delle textView

        float mAccelCurrent = (float)Math.sqrt(valori[0] * valori[0] + valori[1] * valori[1] + valori[2] * valori[2]);

        mAccel= mAccel * 0.9f + mAccelCurrent * 0.1f;

        switch (mSensorLevel) {
            case "1":
                if(mAccel > 9.89 && isOnPasscodeRequire == false) {
                    mAccel = 0;
                    cancelAllRegister();
                    setAllFlagRegistedToFalse();
                    alarm();
                }
                break;
            case "2":
                if(mAccel > 9.99 && isOnPasscodeRequire == false) {
                    mAccel = 0;
                    cancelAllRegister();
                    setAllFlagRegistedToFalse();
                    alarm();
                }
                break;
            case "3":
                if(mAccel > 10.1 && isOnPasscodeRequire == false) {
                    mAccel = 0;
                    cancelAllRegister();
                    setAllFlagRegistedToFalse();
                    alarm();
                }
                break;
        }


    }

//    public BroadcastReceiver mChargerBR = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//
//            if(action.equals(Intent.ACTION_POWER_CONNECTED)) {
//            }
//            else if(action.equals(Intent.ACTION_POWER_DISCONNECTED)) {
//                cancelAllRegister();
//                setAllFlagRegistedToFalse();
//                alarm();
//            }
//        }
//    };


    public BroadcastReceiver mSimcardRemoval = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if(action.equals("android.intent.action.SIM_STATE_CHANGED")) {
                TelephonyManager telephoneMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                int simState = telephoneMgr.getSimState();

                if(simState == TelephonyManager.SIM_STATE_ABSENT) {
                    cancelAllRegister();
                    setAllFlagRegistedToFalse();
                    alarm();
                }
            }
        }
    };


    private void showDialogGotoSetting() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.alert_message_goto_setting)
                .setCancelable(false)
                .setPositiveButton(R.string.yes_titile, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent settingIntent = new Intent(HomeActivity.this,SettingActivity.class);
                        startActivity(settingIntent);
                    }
                })
                .setNegativeButton(R.string.no_titile,null)
                .show();
    }

    private void alarm() {
        if(mAppPrefs == null) {
            mAppPrefs = getSharedPreferences(ConstantValues.APP_PREFERENCES,MODE_PRIVATE);
        }
        int passwordType = mAppPrefs.getInt(ConstantValues.PASSWORD_TYPE, ConstantValues.PASSWORD_TYPE_NONE);
        if(passwordType == ConstantValues.PASSWORD_TYPE_PASSCODE) {
            Intent passcodeIntent = new Intent(HomeActivity.this,PasscodeActivity.class);
            passcodeIntent.putExtra(ConstantValues.PASSWORD_MODE_TYPE,ConstantValues.PASSWORD_MODE_REQUIRED);
            passcodeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            HomeActivity.this.startActivity(passcodeIntent);
            return;
        }
        if(passwordType == ConstantValues.PASSWORD_TYPE_PATTERN) {
            Intent patternIntent = new Intent(HomeActivity.this,PatternActivity.class);
            patternIntent.putExtra(ConstantValues.PASSWORD_MODE_TYPE,ConstantValues.PASSWORD_MODE_REQUIRED);
            patternIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            HomeActivity.this.startActivity(patternIntent);
            return;
        }
    }

    private void unregisterCharger() {
        try {
            if (mChargerBR != null && isChargerActivated)
                unregisterReceiver(mChargerBR);
        }catch (IllegalArgumentException ex) {

        }
    }

    private void unregisterSimcard() {
        try {
            if (mSimcardRemoval != null && isSimcardActivated)
                unregisterReceiver(mSimcardRemoval);
        }catch (IllegalArgumentException ex) {
        }
    }

    private void unregisterSensors() {
        try {
           mSensorManager.unregisterListener(this);
        }catch (IllegalArgumentException ex) {
        }
    }

    private void cancelAllRegister() {

        if(mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        if(materialDialog != null)
            materialDialog.dismiss();

        unregisterCharger();
        unregisterSensors();
        unregisterSimcard();
    }

}
