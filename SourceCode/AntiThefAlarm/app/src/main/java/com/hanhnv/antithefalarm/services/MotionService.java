package com.hanhnv.antithefalarm.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class MotionService extends Service {

    MotionEventListener mMotionEventListener;

    public MotionService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
       return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mMotionEventListener = MotionEventListener.getInstance(getApplicationContext());
        mMotionEventListener.registerSensor();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {

        if(mMotionEventListener != null)
            mMotionEventListener.unregisterSensor();
        super.onDestroy();
    }
}
