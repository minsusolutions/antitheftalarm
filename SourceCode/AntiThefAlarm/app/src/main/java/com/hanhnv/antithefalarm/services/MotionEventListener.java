package com.hanhnv.antithefalarm.services;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

/**
 * Created by sev_user on 6/11/2016.
 */
public class MotionEventListener implements SensorEventListener {

    private static MotionEventListener instance = null;
    private Context mContext;
    private Sensor mAccelerometer;
    private SensorManager mSensorManager;
    float mAccel;


    public static MotionEventListener getInstance(Context context) {
        if(instance == null)
            instance = new MotionEventListener(context);
        return instance;
    }

    public MotionEventListener(Context context) {
        mContext = context;
        mSensorManager = (SensorManager)context.getSystemService(context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mAccel = 0;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void registerSensor() {
        mSensorManager.registerListener(MotionEventListener.this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void unregisterSensor() {
        mSensorManager.unregisterListener(this);
    }


    private void mostraValori(SensorEvent event){
        float[] valori=event.values;//array che contiene i valori dell'accelerometro

        float mAccelCurrent = (float)Math.sqrt(valori[0] * valori[0] + valori[1] * valori[1] + valori[2] * valori[2]);
        mAccel= mAccel * 0.9f + mAccelCurrent * 0.1f;
        Log.d("onSensorChanged", System.currentTimeMillis() + "," + mAccelCurrent + "," + mAccel);
        if(mAccel > 10.80) {
            Intent intent = new Intent();
            intent.setAction("com.hanhnv219.CUSTOM_INTENT");
            mContext.sendBroadcast(intent);
        }
    }
}
