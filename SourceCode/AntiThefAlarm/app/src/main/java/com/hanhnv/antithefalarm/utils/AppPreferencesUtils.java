package com.hanhnv.antithefalarm.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by sev_user on 3/25/2016.
 */
public class AppPreferencesUtils {
    private Context mContext;
    private static AppPreferencesUtils instance = null;
    private static SharedPreferences mAppPrefs;

    private AppPreferencesUtils (Context context) {
        mContext = context;
        mAppPrefs = mContext.getSharedPreferences(ConstantValues.APP_PREFERENCES,mContext.MODE_PRIVATE);
    }

    public static AppPreferencesUtils getInstance (Context context) {
        if(instance == null)
            instance = new AppPreferencesUtils(context);
        return instance;
    }

    public SharedPreferences.Editor getPrefEditor() {
        return mAppPrefs.edit();
    }





}
