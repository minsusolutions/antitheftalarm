package com.hanhnv.antithefalarm.utils;

import android.content.Context;
import android.content.Intent;

import com.hanhnv.antithefalarm.activities.PasscodeActivity;

/**
 * Created by sev_user on 6/11/2016.
 */
public class Utils {

    private Context mContext;

    private static Utils instance = null;

    public static Utils getInstance(Context context) {
        if(instance == null)
            instance = new Utils(context);
        return instance;
    }

    public Utils (Context context) {
        mContext = context;
    }

    private void alarm() {
        Intent intent = new Intent(mContext,PasscodeActivity.class);
        mContext.startActivity(intent);
    }

}
