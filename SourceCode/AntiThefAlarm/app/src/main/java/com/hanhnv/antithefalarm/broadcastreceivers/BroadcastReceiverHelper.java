package com.hanhnv.antithefalarm.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.hanhnv.antithefalarm.utils.ConstantValues;

/**
 * Created by sev_user on 6/11/2016.
 */
public class BroadcastReceiverHelper  extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(ConstantValues.ACTION_PASSWORD_MODE_REQUIRE)) {
            Toast.makeText(context,"Password mode require",Toast.LENGTH_LONG).show();
        }
    }
}
